/**
 script.js
 */

let js = document.getElementsByClassName('no-js');
for(let element of js ){
    element.classList.remove('no-js');
    element.classList.add('js');
}

document.documentElement.classList.add('js');