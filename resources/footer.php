<?php

/*
 * Copyright 2021 Dragon Media Group.
 * All rights reserved. Modification of this file may only be done using written permission.
 */

use Timber\Timber;

$timberContext = $GLOBALS['timberContext']; // @codingStandardsIgnoreFile
if (!isset($timberContext)) {
    throw new Exception('Timber context not set in footer.');
}
$timberContext['content'] = ob_get_contents();
ob_end_clean();
$templates = ['!page-plugin.twig'];
Timber::render($templates, $timberContext);
