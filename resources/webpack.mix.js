const mix = require('laravel-mix');
const path = require('path');
const fs = require('fs');

mix.autoload({
    jquery: ['$', 'jQuery', 'window.jQuery']
});
mix.webpackConfig({
    externals: {
        "jquery": "jQuery"
    }
});


mix.options({
    uglify: {
        uglifyOptions: {
            compress: {
                drop_console: true
            }
        }
    }
});

mix.extract([
    'jquery',
    'popper.js',
    'bootstrap'
]);

/**
 *
 * @param {string|string[]} files
 * @param {string} source_dir
 * @param {string} target_dir
 */
function register(files, source_dir, target_dir) {
    if (typeof files === 'string') {
        files = files.split(/\s+/);
    }
    files.forEach((block) => {
        let source;
        let target = target_dir + block + '.css';

        if (fs.existsSync(source = source_dir + block + '.scss')) {
            mix.sass(source, target);
        }

        // jsx before js
        target = target_dir + block + '.js';
        if (fs.existsSync(source = source_dir + block + '.jsx')) {
            mix.react(source, target);
        } else {
            if (fs.existsSync(source = source_dir + block + '.js')) {
                mix.js(source, target);
            }
        }
    });
}

register('dummy', 'source/blocks/', 'dist/blocks/');

mix.version();

mix.sass('source/style.scss', 'dist');
mix.js('source/script.js', 'dist');

// mix.browserSync('mysite.localhost');

// Override the default path to your project's public directory.
mix.setPublicPath('./dist/');
// Set a prefix for all generated asset paths.
mix.setResourceRoot('/wp-content/themes/' + path.basename(__dirname) + '/dist/');