<?php

/*
 * Copyright 2021 Dragon Media Group.
 * All rights reserved. Modification of this file may only be done using written permission.
 */

use Composer\Autoload\ClassLoader;
use DMG\Wordpress\Template\Initializer;

$autoloader = require __DIR__.'/vendor/autoload.php';

// load the adhoc classes before the base classes.
if ($autoloader instanceof ClassLoader) {
    $autoloader->addPsr4('DMG\Wordpress\Template\\', __DIR__.'/classes_adhoc', true);
}

Initializer::initialize();
