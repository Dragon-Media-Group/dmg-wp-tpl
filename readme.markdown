Install
=======

1. make composer.json. require-dev is optional but has a better debugger for twig `{{ dump() }`

        {
          "name": "dmg/mywebsite",
          "license": "proprietary",
          "repositories": [
            {
              "type": "vcs",
              "url": "https://myusername@bitbucket.org/Dragon-Media-Group/dmg-wp-tpl.git"
            },
            {
              "type":"composer",
              "url":"https://wpackagist.org"
            }
          ],
          "require-dev": {
            "hellonico/twig-dump-extension": "^1.0"
          }
        }
    
    Then:
    
        composer install

2. add dependency. Using git clone (dev-master so it can be edited in vendor folder):

        composer require dmg/wordpress-template:dev-master --prefer-source --dev
   
   or stable:

        composer require dmg/wordpress-template

3. copy the default resources:
        
   Windows
   -------

        xcopy vendor\dmg\wordpress-template\resources . /e
        
   linux (untested!)
   -----

        cp vendor/dmg/wordpress-template/resources . --recursive

4.  install laravel mix. By default, also bootstrap and popper.js are included to dependencies. They can be removed from `package.json`
        
        npm install

5.  run laravel mix

        npm run <command>
        
    where `command` equals `dev`, `watch`, `hot`, `production`.
    
6.  define `WP_SPACE_ALLOWED` in wp-config. This is the quota of the number of megabytes allowed to upload media.
