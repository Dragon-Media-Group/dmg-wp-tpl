<?php

/*
 * Copyright 2021 Dragon Media Group.
 * All rights reserved. Modification of this file may only be done using written permission.
 */

use DMG\Wordpress\Template\Site;

function is_blog_homepage()
{
    return Site::BLOG_HOMEPAGE === Site::getFrontPageOrBlogType();
}

function is_static_homepage()
{
    return Site::STATIC_HOMEPAGE === Site::getFrontPageOrBlogType();
}

function is_static_blog()
{
    return Site::STATIC_BLOG === Site::getFrontPageOrBlogType();
}

if (!defined('PHP_VERSION_ID')) {
    $version = explode('.', PHP_VERSION);

    define('PHP_VERSION_ID', $version[0] * 10000 + $version[1] * 100 + $version[2]);
}

if (!function_exists('debug_print_backtrace_pre')) {
    function debug_print_backtrace_pre(Exception $e = null)
    {
        echo '<pre style="white-space:pre-wrap">';
        if (null === $e) {
            call_user_func_array('debug_print_backtrace', func_get_args());
        } else {
            echo $e->getTraceAsString();
        }
        echo '</pre>';
    }
}

if (!function_exists('dump')) {
    function dump()
    {
        if (WP_DEBUG && WP_DEBUG_DISPLAY) {
//                    var_dump(func_get_args());
            foreach (func_get_args() as $i => $arg) {
                echo '<h3>arg ', $i + 1, '</h3>';
                var_dump($arg);
            }
        }
    }
}
// same as date_i18n, but this adds the offset.
if (!function_exists('date_i18n_local')) {
    function date_i18n_local($format, $timestamp)
    {
        if ($timestamp instanceof \DateTime) {
            $timestamp = $timestamp->getTimestamp();
        }

        $timezone_str = get_option('timezone_string') ?: 'UTC';
        $timezone = new \DateTimeZone($timezone_str);

        // The date in the local timezone.
        $date = new \DateTime(null, $timezone);
        $date->setTimestamp($timestamp);
        $date_str = $date->format('Y-m-d H:i:s');

        // Pretend the local date is UTC to get the timestamp
        // to pass to date_i18n().
        $utc_timezone = new \DateTimeZone('UTC');
        $utc_date = new \DateTime($date_str, $utc_timezone);
        $timestamp = $utc_date->getTimestamp();

        return date_i18n($format, $timestamp, true);
    }
}
