<?php

/*
 * Copyright 2021 Dragon Media Group.
 * All rights reserved. Modification of this file may only be done using written permission.
 */

class_alias('HelloNico\Twig\DumpExtension', 'DMG\Wordpress\Template\Twig\DumpExtension');
