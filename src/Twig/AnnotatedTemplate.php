<?php

/*
 * Copyright 2021 Dragon Media Group.
 * All rights reserved. Modification of this file may only be done using written permission.
 */

namespace DMG\Wordpress\Template\Twig;

abstract class AnnotatedTemplate extends \Twig\Template
{
    protected static $html_printed = false;

    /**
     * @throws \Throwable
     *
     * @return false|string
     */
    public function render(array $context)
    {
        $level = ob_get_level();
        ob_start();

        try {
            $this->display($context);
        } catch (Exception $e) {
            while (ob_get_level() > $level) {
                ob_end_clean();
            }
            if (WP_DEBUG && WP_DEBUG_DISPLAY) {
                //				var_dump( $context, $e );
            }

            throw $e;
        }
        $content = ob_get_clean();

        $start_pos = stripos($content, '<head>');
        if (false === $start_pos) {
            $start_pos = stripos($content, '<html>');
            if (false === $start_pos) {
                $start_pos = stripos($content, '<head ');
                if (false === $start_pos) {
                    $start_pos = stripos($content, '<html ');
                }
            }
        }
        if (false === $start_pos) {
            $content = '<!-- START: template '.$this->getTemplateName().' -->'.PHP_EOL.ltrim($content);
        } else {
            $content = rtrim(substr($content, 0, $start_pos)).PHP_EOL
                .'<!-- START: template '.$this->getTemplateName().' -->'.PHP_EOL
                .ltrim(substr($content, $start_pos));
        }

        $end_pos = stripos($content, '</body>');
        if (false === $end_pos) {
            $end_pos = stripos($content, '</html>');
        }
        if (false === $end_pos) {
            $content .= PHP_EOL.'<!-- END: template '.$this->getTemplateName().' -->';
        } else {
            $content = rtrim(substr($content, 0, $end_pos)).PHP_EOL
                .'<!-- END: template '.$this->getTemplateName().' -->'.PHP_EOL
                .ltrim(substr($content, $end_pos));
        }

        return $content;
    }

    /**
     * @param string              $name
     * @param bool                $useBlocks
     * @param null|\Twig_Template $templateContext
     *
     * @throws \Exception
     */
    public function displayBlock(
        $name,
        array $context,
        array $blocks = [],
        $useBlocks = true,
        Twig_Template $templateContext = null
    ) {
        $level = ob_get_level();
        $e = null;
        ob_start();

        try {
            parent::displayBlock($name, $context, $blocks, $useBlocks);
        } catch (Exception $e) {
            while (ob_get_level() > $level) {
                ob_end_clean();
            }

            throw $e;
        }
        $content = ob_get_clean();
        echo PHP_EOL,
            '<!-- START: '.$this->getTemplateName().'::'.$name.'-->', PHP_EOL,
        trim($content), PHP_EOL,
            '<!-- END: '.$this->getTemplateName().'::'.$name.'-->', PHP_EOL;
    }

    public function loadTemplate($template, $templateName = null, $line = null, $index = null)
    {
        $template = parent::loadTemplate($template, $templateName, $line, $index);

        echo "\n<!-- TEMPLATE FOUND: {$template->getTemplateName()}-->";

        return $template;
    }
}
