<?php

/*
 * Copyright 2021 Dragon Media Group.
 * All rights reserved. Modification of this file may only be done using written permission.
 */

namespace DMG\Wordpress\Template;

use Exception;
use LogicException;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Timber\MenuItem;
use Timber\Timber;
use Twig\Environment;
use Twig\TwigFunction;

/**
 * STUB CLASS! DO NOT ADD METHODS!
 */
abstract class AtlantisBase
{
    /**
     * @var string
     */
    protected $url_publieksomgeving;

    /**
     * Adds css classes to nav items to conditionally hide/show items.
     *
     * @var bool
     */
    protected $login_modifier_css_class_support = false;

    /**
     * AtlantisBase constructor.
     */
    public function __construct()
    {
        if (!defined('URL_ATLANTIS_PUBLIEKSOMGEVING')) {
            throw new LogicException('constant URL_ATLANTIS_PUBLIEKSOMGEVING not defined. set it in functions.php.');
        }
        $this->url_publieksomgeving = rtrim(URL_ATLANTIS_PUBLIEKSOMGEVING, '/').'/';

        add_shortcode('atlantis', [$this, 'shortcodeAtlantisUrl']);
        add_shortcode('atlantis_url', [$this, 'shortcodeAtlantisUrl']);
        add_shortcode('atlantis_keuzelijst_zoeken', [$this, 'shortcodeAtlantisKeuzelijstZoeken']);
    }

    public function shortcodeAtlantisKeuzelijstZoeken($atts = null, $content = null, $tag = '')
    {
        $atts = shortcode_atts([
            'veld' => 'collectie',
            'beschrijvingssoorten' => null,
            'groep' => null,
            'beginindex' => 0,
            'eindindex' => -1,
            'highlight' => null,
            'als_filter' => null,
            'cachetime' => 86400,
        ], $atts, $tag);

        if (is_string($atts['highlight'])) {
            switch (strtolower($atts['highlight'])) {
                case 'true':
                case 't':
                case 'yes':
                case 'y':
                case 'ja':
                case 'j':
                    $atts['highlight'] = true;

                    break;
                case 'false':
                case 'f':
                case 'no':
                case 'n':
                case 'nee':
                    $atts['highlight'] = false;

                    break;
                default:
                    $atts['highlight'] = (bool) filter_var(FILTER_VALIDATE_BOOLEAN, $atts['highlight']);

                    break;
            }
        }

        $cached = $this->loadCachedAtlantisServiceResponse('Search', 'GetKeuzeVoorZoekveld', $params = [
            'fieldname' => $atts['veld'],
            'beschrijvingssoorten' => $atts['beschrijvingssoorten'],
            'beschrijvingsgroepen' => $atts['groep'],
            'beginindex' => $atts['beginindex'],
            'eindindex' => $atts['eindindex'],
        ], $atts['cachetime'], true);

        $keuzelijst_query = [
            // see SearchModel::view
            'auto_beschrijvingssoorten' => 'true',
        ];

        if (null !== $atts['beschrijvingssoorten']) {
            unset($keuzelijst_query['auto_beschrijvingssoorten']);
            $keuzelijst_query['beschrijvingssoorten'] = explode(',', $atts['beschrijvingssoorten']);
        }
        if (null !== $atts['highlight']) {
            $keuzelijst_query['highlight'] = $atts['highlight'] ? 't' : 'f';
        }

        if (isset($cached['url'], $cached['modified'])) {
            $content .= sprintf(
                '<!-- LocalSearchModel::GetKeuzeVoorZoekveld(%s) -->'.PHP_EOL,
                __METHOD__,
                var_export(
                    $params,
                    true
                )
            );
            $content .= sprintf('<!-- cached: %s from %s-->'.PHP_EOL, $cached['modified'], $cached['url']);
            if (isset($cached['data']['RECORD']) && is_array($cached['data']['RECORD'])) {
                $links = [];

                foreach ($cached['data']['RECORD'] as $record) {
                    if (isset($record['RESULT'])) {
                        $query = $keuzelijst_query;

                        if (null !== $atts['als_filter']) {
                            $query['zoeken']['filters'][$atts['als_filter']] = [$record['RESULT']];
                        }
                        $query['zoeken']['velden'][$atts['veld']]['waarde'] = $record['RESULT'];

                        $query = http_build_query($query);
                        $query = htmlspecialchars($query);

                        $links[] = [
                            'url' => sprintf(
                                '%szoeken.php?%s',
                                $this->url_publieksomgeving,
                                $query
                            ),
                            'title' => $record['RESULT'],
                        ];
                    }
                }

                $content .= Timber::compile(
                    'shortcodes/atlantis/keuzelijst_zoeken.twig',
                    compact('links', 'params', 'cached', 'atts')
                );
            }
        }

        return $content;
    }

    public function shortcodeAtlantisUrl($atts = null, $content = null, $tag = '')
    {
        return $this->url_publieksomgeving;
    }

    /**
     * @filter nav_menu_link_attributes
     *
     * @return array
     */
    public function urlPublieksOmgevingInNavItemHref(array $attributes)
    {
        if (!is_admin()) {
            $attributes['href'] = preg_replace(
                '@^.*\[atlantis\]/*@i',
                $this->url_publieksomgeving,
                $attributes['href']
            );
        }

        return $attributes;
    }

    /**
     * @filter nav_menu_css_class
     *
     * @param string[]          $classes
     * @param MenuItem|\WP_Post $item
     *
     * @return string[]
     */
    public function atlantisLoginModifierInNaviItemClasses(array $classes, $item)
    {
        if (!$this->login_modifier_css_class_support || !is_object($item)
            || !($item instanceof \WP_Post || $item instanceof MenuItem)) {
            return $classes;
        }

        return array_unique(array_merge($classes, get_field('atlantis_login_modifier_classes', $item->ID) ?: []));
    }

    /**
     * Adds checkboxes so you can conditionally hide a menu item.
     *
     * @action acf/init
     */
    public function atlantisLoginModifierFields()
    {
        if (!$this->login_modifier_css_class_support) {
            return;
        }

        // https://github.com/StoutLogic/acf-builder/wiki
        $fields = new FieldsBuilder('atlantis_login_modifier_fields', [
            'title' => 'Publieksomgeving menu item velden',
        ]);

        $fields->setLocation('nav_menu_item', '==', 'all');

        $fields->addCheckbox('atlantis_login_modifier_classes', [
            'label' => 'Inlogstatus',
            'choices' => [
                'atlantis-not-logged-in--hide' => '<strong style="background-color:#ccc">Verbergen</strong> als bezoeker <strong style="color:red">niet</strong> op publieksomgeving ingelogd is.',
                'atlantis-not-logged-in--disabled' => '<strong style="background-color:#666;color:#fff;">Uitschakelen</strong> als bezoeker <strong style="color:red">niet</strong> op publieksomgeving ingelogd is (het item is 50% doorzichtig en niet klikbaar).',

                'atlantis-is-logged-in--hide' => '<strong style="background-color:#ccc">Verbergen</strong> als bezoeker <strong style="color:green">wel</strong> op publieksomgeving ingelogd is.',
            ],
        ])->setInstructions('Weergeven/verbergen van menu items op basis van inlogstatus');

//        dump( $fields->build() );

        acf_add_local_field_group($fields->build());
    }

    /**
     * @filter timber/context
     *
     * @return array
     */
    public function timberContext(array $context)
    {
        $context['url_atlantis_publieksomgeving'] = $this->url_publieksomgeving;

        return $context;
    }

    /**
     * @filter timber/twig/filters
     *
     * @return Environment
     */
    public function twigFunctions(Environment $twig)
    {
        $twig->addFunction(new TwigFunction('atlantis_werksets', [$this, 'GetWerksets']));
        $twig->addFunction(new TwigFunction('atlantis_werkset', [$this, 'GetWerkset']));
        $twig->addFunction(new TwigFunction('atlantis_dagselectie', [$this, 'GetSelectieVanDeDag']));
        $twig->addFunction(new TwigFunction('atlantis_werksetnaam', [$this, 'GetWerksetNaam']));
        $twig->addFunction(new TwigFunction('atlantis_globalsearch', [$this, 'GetGlobalSearch']));
        $twig->addFunction(new TwigFunction('atlantis_extendedsearch', [$this, 'GetExtendedSearch']));

        return $twig;
    }

    public function GetGlobalSearch(array $params = [])
    {
        $query = build_query($params);
        if ('' !== $query) {
            $query = '?'.$query;
        }

        return RemoteLoader::loadCached($this->url_publieksomgeving.'GlobalSearchTemplate'.$query, 600);
    }

    public function GetExtendedSearch(array $params = [])
    {
        $query = build_query($params);
        if ('' !== $query) {
            $query = '?'.$query;
        }

        return RemoteLoader::loadCached($this->url_publieksomgeving.'ExtendedSearchTemplate'.$query, 600);
    }

    /**
     * @filter acf/load_field
     */
    public function loadFieldValues(array $field)
    {
        if (!empty($field['wrapper']['class'])) {
            $loaded = true;
            switch ($field['wrapper']['class']) {
                case 'atlantis_werkset':
                case 'atlantis_werksets':
                    $field = $this->loadWerksetsToACFField($field);

                    break;
//                case 'atlantis_beschrijvingsgroep':
//                case 'atlantis_beschrijvingsgroepen':
//                    $field['instructions'] =
//                    break;
                default:
                    $loaded = false;

                    break;
            }
            if ($loaded) {
                $field['ui'] = true;
//                if()
            }
        }

        return $field;
    }

    /**
     * @param bool $entire_item
     * @param int  $cache_time
     *
     * @throws Exception
     *
     * @return array|mixed|object
     */
    public function GetWerksets($entire_item = false, $cache_time = 900)
    {
        return $this->loadCachedAtlantisServiceResponse(
            'Workset',
            'GetWerksets',
            [],
            $cache_time,
            $entire_item
        );
    }

    public function GetSelectieVanDeDag($name, $cache_time = 900, $limit = 100, $offset = 0)
    {
        return $this->GetWerkset($name, $cache_time, __FUNCTION__, $limit, $offset);
    }

    /**
     * @param                  $name
     * @param int|string       $cache_time
     * @param string           $fn
     * @param float|int|string $limit
     * @param float|int|string $offset
     *
     * @throws Exception
     *
     * @return array|mixed|object
     */
    public function GetWerkset($name, $cache_time = 900, $fn = 'GetWerkset', $limit = 100, $offset = 0)
    {
        if (is_numeric($name)) {
            $werksets = $this->GetWerksets(false, $cache_time);
            if (isset($werksets[$name])) {
                $name = $werksets[$name];
            } else {
                return [];
            }
        }

        $response = $this->loadCachedAtlantisServiceResponse(
            'Workset',
            $fn,
            compact('name', 'limit', 'offset'),
            $cache_time,
            false
        );

        if (!is_array($response)) {
            $response = [];
        }

        return array_filter($response);
    }

    /**
     * @throws Exception
     *
     * @return array
     */
    protected function loadWerksetsToACFField(array $field)
    {
        $werksets = $this->GetWerksets(true, 60 * 15);

        $url = htmlspecialchars($werksets['url']);
        if (is_wp_error($werksets['response'])) {
            $field['instructions'] = "Fout bij ophalen van werksets vanaf {$werksets['url']}: {$werksets['response']->get_error_message()}.";
        } else {
            if (is_array($werksets['data'])) {
                $field['instructions'] = 'Kies een werkset uit atlantis.';

                $field['choices'] = $werksets['data'];

//                if (WP_DEBUG) {
                $field['instructions'] .= ' Links naar rauwe data: ';
                $last_index = count($werksets['data']) - 1;
                foreach (array_values($werksets['data']) as $index => $werkset) {
                    if (0 !== $index) {
                        if ($index !== $last_index) {
                            $field['instructions'] .= ', ';
                        } else {
                            $field['instructions'] .= ' en ';
                        }
                    }
                    $encoded = htmlspecialchars(urlencode($werkset));
                    $ht_werkset = htmlspecialchars($werkset);
                    $field['instructions'] .= "<a target='_blank' href='{$this->url_publieksomgeving}service.php?service=Workset&amp;function=GetWerkset&amp;params[name]={$encoded}'>{$ht_werkset}</a>";
                }
                $field['instructions'] .= '';
//                }
            } elseif (!empty($werksets['decode_error'])) {
                $field['instructions'] = "Er ging iets fout bij het laden van de werksets: {$werksets['decode_error']}.";
            } else {
                $field['instructions'] = 'Er ging iets mis bij het laden van de werksets. Data is leeg.';
            }
        }

        $relative = human_time_diff($werksets['timestamp'], time());

        $field['instructions'] .= "<br>Leeftijd van <a href='{$url}' target='_blank'>werkset namen</a>: {$relative} - {$werksets['modified']}.";

        return $field;
    }

    /**
     * @param      $service
     * @param      $function
     * @param      $params
     * @param int  $cachetime
     * @param bool $entire_item
     * @param bool $json
     *
     * @throws \Exception
     *
     * @return array|mixed|object
     */
    protected function loadCachedAtlantisServiceResponse(
        $service,
        $function,
        $params,
        $cachetime = 86400,
        $entire_item = false,
        array $remote_get_args = [],
        $json = false
    ) {
        $params = wp_parse_args($params);
        $query = http_build_query(compact('service', 'function', 'params'));
        if (empty($params)) {
            $query .= '&params=[]';
        }
        $url = $this->url_publieksomgeving.'service.php?'.$query;

        $md5 = md5($url);
        $cache_key = "{$service}::{$function}({$md5})";

        return RemoteLoader::loadCached($url, $cachetime, $entire_item, $remote_get_args, $cache_key, $json);
    }
}
