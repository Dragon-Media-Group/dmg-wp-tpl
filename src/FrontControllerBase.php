<?php

/*
 * Copyright 2021 Dragon Media Group.
 * All rights reserved. Modification of this file may only be done using written permission.
 */

namespace DMG\Wordpress\Template;

use Brain\Hierarchy\Hierarchy;
use Timber\Helper as TimberHelper;
use Timber\Timber;

abstract class FrontControllerBase
{
    public static function run()
    {
        $start = TimberHelper::start_timer();

        $hierarchy = apply_filters('timber/hierarchy', new Hierarchy());

        $context = apply_filters('timber/context/with_hierarchy', Timber::context(), $hierarchy);

        $templates = $hierarchy->getTemplates();

        if ($frontpage_or_blog_type = Site::getFrontPageOrBlogType()) {
            array_unshift($templates, $frontpage_or_blog_type);
        }

        if (apply_filters('do_remove_home_blog_frontpage_body_templates', true)) {
            $templates = array_combine($templates, $templates);
            unset($templates['front-page'], $templates['blog']);
        }

        // If a page template was specified, it must be loaded BEFORE front-page etc...
        $template_slug = get_page_template_slug(get_queried_object_id());
        if ($template_slug && 'twig' === pathinfo(strtolower($template_slug), PATHINFO_EXTENSION)) {
            array_unshift($templates, pathinfo($template_slug, PATHINFO_FILENAME));
            $templates = array_combine($templates, $templates);
        }

        $templates = apply_filters('timber/frontcontroller/templates', $templates);
        $templates = array_map(static function ($template) {
            return $template.'.twig';
        }, $templates);

        $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 1);
        $caller = array_shift($trace);

        $context['frontcontroller']['caller'] = $caller;
        $context['frontcontroller']['template'] = '';
        $context['frontcontroller']['templates'] = $templates;

        // add the current twig file to the theme.
        add_filter('timber_render_file', $a = static function ($template) {
            add_filter('timber_render_data', $a = static function (array $context) use ($template) {
                $context['frontcontroller']['template'] = $template;

                return $context;
            });

            return $template;
        });
        add_filter('timber_compile_file', $a = static function ($template) {
            add_filter('timber_compile_data', $a = static function (array $context) use ($template) {
                $context['frontcontroller']['template'] = $template;

                return $context;
            });

            return $template;
        });

        Timber::render(
            $templates,
            $context
        );

        if (WP_DEBUG && WP_DEBUG_DISPLAY) {
            echo PHP_EOL, '<pre>Timber: ', PHP_EOL,
            'date: ', date('c'), PHP_EOL,
            'generated from ', __FILE__, ' in ', TimberHelper::stop_timer($start),
            '</pre>';
        }
    }
}
