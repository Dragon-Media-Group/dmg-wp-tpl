<?php

/*
 * Copyright 2021 Dragon Media Group.
 * All rights reserved. Modification of this file may only be done using written permission.
 */

namespace DMG\Wordpress\Template\ACF;

abstract class MenuFieldTypeBase extends \acf_field
{
    public function __construct()
    {
        $this->name = 'dmg-nav-menu';
        $this->label = 'Menu';
        $this->category = 'content';

        parent::__construct();
    }

    public function render_field(array $field)
    {
        // Nav Menus
        $nav_menus = $this->get_nav_menus();
        if ($nav_menus) {
            echo sprintf(
                '<select id="%d" class="%s" name="%s" required>',
                $field['id'],
                $field['class'],
                $field['name']
            );
            echo '<option value="" disabled="disabled">Selecteer&hellip;</option>';
            foreach ($nav_menus as $nav_menu_id => $nav_menu_name) {
                $selected = selected($field['value'], $nav_menu_id);
                echo sprintf('<option value="%1$d" %3$s>%2$s</option>', $nav_menu_id, $nav_menu_name, $selected);
            }
            echo '</select>';
        } else {
            printf(
                '<p>Er zijn nog geen menu\'s. <a class="acf-button button button-primary" href="%s">Maak een menu aan</p>',
                admin_url('nav-menus.php')
            );
        }
    }

    public function get_nav_menus()
    {
        $navs = get_terms('nav_menu', ['hide_empty' => false]);

        $nav_menus = [];
        foreach ($navs as $nav) {
            $nav_menus[$nav->term_id] = $nav->name;
        }

        return $nav_menus;
    }
}
