<?php

/*
 * Copyright 2021 Dragon Media Group.
 * All rights reserved. Modification of this file may only be done using written permission.
 */

namespace DMG\Wordpress\Template\ACF;

abstract class ForkAwesomeFieldTypeBase extends \acf_field
{
    protected $is_latest_fork_awesome_version;
    protected $fork_awesome_version;
    protected $stylesheet_url;

    public function __construct()
    {
        $this->name = 'dmg-fork-awesome';
        $this->label = 'Fork Awesome Icoon';
        $this->category = 'content';

        $this->fork_awesome_version = apply_filters('dmg/fork_awesome_version', 'latest');

        if ('latest' === $this->fork_awesome_version) {
            $this->is_latest_fork_awesome_version = true;
            $this->stylesheet_url = 'https://unpkg.com/fork-awesome/css/fork-awesome.min.css';

            // save version in session
            if (!isset($_SESSION['latest-fork-awesome-version'])) {
                $response = wp_remote_get($this->stylesheet_url);
                $body = wp_remote_retrieve_body($response);
                if (preg_match('@\?v=(\d+\.\d+\.\d+)@', $body, $m)) {
                    $this->fork_awesome_version = $m[1];
                    $_SESSION['latest-fork-awesome-version'] = $m[1];
                }
            }
        } else {
            $this->stylesheet_url = sprintf(
                'https://unpkg.com/fork-awesome@%s/css/fork-awesome.min.css',
                $this->fork_awesome_version
            );
        }

        parent::__construct();
    }

    /**
     * @param mixed[] $field
     *
     * @see \acf_field_radio::render_field()
     */
    public function render_field(array $field)
    {
        $icon_names = $this->icon_names();

        $choices = [];
        if (!$field['required']) {
            $choices[''] = 'Geen icoon';
        }
        foreach ($icon_names as $icon_name) {
            $choices[$icon_name] = $icon_name;
        }

        $value = (string) $field['value'];

        if (!isset($choices[$value])) {
            $value = (string) key($choices);
        }

        acf_select_input([
            'choices' => $choices,
            'value' => $value,
            'id' => $field['id'],
            'name' => $field['name'],
            'class' => $field['class'].' dmg-fork-awesome-select',
        ]);
    }

    public function icon_names()
    {
        // _variables.scss
        // https://unpkg.com/fork-awesome/scss/_variables.scss

        if (!isset($_SESSION['fork-awesome-names'])) {
            $icons = [];

            $response = wp_remote_get($this->stylesheet_url);
            $body = wp_remote_retrieve_body($response);

            // https://regex101.com/r/1UilhA/1
            preg_match_all('/\.fa-(?<name>[\w-]+):before/m', $body, $m);
            if (isset($m['name'])) {
                $icons = $m['name'];
                sort($icons);
            }
            $_SESSION['fork-awesome-names'] = $icons;
        }

        return $_SESSION['fork-awesome-names'];
    }

    public function input_admin_enqueue_scripts()
    {
        // todo: clean this :)
        wp_register_style(
            'forkawesome',
            $this->stylesheet_url,
            [],
            $this->fork_awesome_version
        );
        wp_enqueue_style('forkawesome');

        $register_field_type_script = <<<'JS'
(function($, type, undefined){
	
    // dmg fork awesome icon selector 
    // inline script added to "acf-input"
    
	var Field = acf.Field.extend({
		
		type: type,
		
		select2: false,
		
		wait: 'load',
		
		events: {
			'removeField': 'onRemove'
		},
		
		$input: function(){
			return this.$('select');
		},
		
		initialize: function(){
			
			// vars
			var $select = this.$input();
			
			// inherit data
			this.inherit( $select );
			
            this.select2 = acf.newSelect2($select, {
                field: this
            });

		},
		
		onRemove: function(){
			if( this.select2 ) {
				this.select2.destroy();
			}
		}
	});
	
	var FontAwesomeStateFormatter = function(state) {
	     if (!state.id || typeof state.id === 'string' && state.id.length === 0) {
	         return state.text;
         }
	     return '<big><i class="fa fa-fw fa-'+state.id+'" aria-hidden="true"></i></big> '+state.id;
	};
	
	// add html support to select2 for this type.
	// https://select2.org/dropdown
	acf.addFilter('select2_args', function (options, $select) {
	    if($select.is('select.dmg-fork-awesome-select')) {
	        // https://select2.org/dropdown#templating
	        options.templateResult = FontAwesomeStateFormatter;
	        // https://select2.org/selections#templating
	        options.templateSelection = FontAwesomeStateFormatter;
	    }
        return options;	    
	});
	
	acf.registerFieldType( Field );
	
})
JS;
        $register_field_type_script .= sprintf('(jQuery, \'%s\');', $this->name);

        wp_add_inline_script('acf-input', $register_field_type_script);
    }
}
