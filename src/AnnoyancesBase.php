<?php

/*
 * Copyright 2021 Dragon Media Group.
 * All rights reserved. Modification of this file may only be done using written permission.
 */

namespace DMG\Wordpress\Template;

class AnnoyancesBase
{
    /**
     * @filter use_block_editor_for_post
     *
     * @return bool
     */
    public function isGutenbergEnabled()
    {
        return false;
    }

    /**
     * @filter wpcf7_autop_or_not
     *
     * @return bool
     */
    public function isWPContactForms7UsingParagraphs()
    {
        return false;
    }

    /**
     * @filter sanitize_file_name
     *
     * @param string $file_name
     *
     * @return string
     */
    public function sanitizeFileName($file_name = '')
    {
        // Empty filename
        if (empty($file_name)) {
            return $file_name;
        }

        // get extension
        $ext = (string) pathinfo($file_name, PATHINFO_EXTENSION);

        // work only on the filename without extension
        $name = sanitize_title((string) pathinfo($file_name, PATHINFO_FILENAME), md5(uniqid(time(), true)));

        if ('' !== $ext) {
            $name .= '.'.strtolower($ext);
        }

        // Return sanitized file name
        return $name;
    }
}
