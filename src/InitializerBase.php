<?php

/*
 * Copyright 2021 Dragon Media Group.
 * All rights reserved. Modification of this file may only be done using written permission.
 */

namespace DMG\Wordpress\Template;

use Timber\Timber;

abstract class InitializerBase
{
    /**
     * @throws \Exception
     */
    public static function initialize()
    {
        static::undefinedFunctionsCheck();

        if (static::hasRequirements()) {
            //			Timber::$autoescape = true;
            // https://timber.github.io/docs/guides/performance/#cache-the-twig-file-but-not-the-data
            Timber::$cache = true;

            $cache_it = !WP_DEBUG;

            $hooker = new Hooker();
            $hooker->register(new Site(), $cache_it);
            $hooker->register(new TwigHooks(), $cache_it);
            $hooker->register(new ACFHooks(), $cache_it);
            $hooker->register(new Annoyances(), $cache_it);
            $hooker->register(new ScriptStylesLoader(), $cache_it);

            if (defined('URL_ATLANTIS_PUBLIEKSOMGEVING')) {
                $hooker->register(new Atlantis(), $cache_it);
            }

            // init!!
            new Timber();
        }
    }

    protected static function undefinedFunctionsCheck()
    {
    }

    /**
     * @return bool
     */
    protected static function hasRequirements()
    {
        if (PHP_VERSION_ID < 70200) {
            add_action('admin_notices', function () {
                echo '<div class="error"><p>Wrong PHP version!! requires PHP 7.2</a></p></div>';
            });
            add_filter('template_include', function () {
                return get_theme_file_path('static/no-php7.html');
            });

            return false;
        }

        require_once ABSPATH.'wp-admin/includes/plugin.php';

        if (!class_exists('Timber\Timber') && !is_plugin_active('timber-library/timber.php')) {
            add_action('admin_notices', function () {
                echo '<div class="error"><p>Timber not activated. Make sure you <a href="'
                    .esc_url(admin_url('plugins.php?s=timber#timber'))
                    .'">activate the plugin</a>.</p></div>';
            });
            add_filter('template_include', function () {
                return get_theme_file_path('static/no-timber.html');
            });

            return false;
        }

        if (!is_plugin_active('advanced-custom-fields-pro/acf.php')) {
            add_action('admin_notices', function () {
                echo '<div class="error"><p>ACF Pro not activated. Make sure you <a href="'
                    .esc_url(admin_url('plugins.php?s=acf')).'">activate the plugin</a>.</p></div>';
            });
            add_filter('template_include', function () {
                return get_theme_file_path('static/no-acf-pro.html');
            });

            return false;
        }

        return true;
    }
}
