<?php

/*
 * Copyright 2021 Dragon Media Group.
 * All rights reserved. Modification of this file may only be done using written permission.
 */

namespace DMG\Wordpress\Template;

use Exception;

class RemoteLoaderBase
{
    protected const USE_TRANSIENTS = true;

    /**
     * @param string $url
     * @param bool   $entire_item
     * @param array  $remote_get_args
     *
     * @throws Exception
     *
     * @return array|mixed|object
     */
    public static function load($url, $entire_item = false, $remote_get_args = [])
    {
        return static::loadCached($url, false, $entire_item, $remote_get_args);
    }

    /**
     * @param string     $url
     * @param int|string $cachetime
     * @param bool       $entire_item
     * @param array      $remote_get_args
     * @param null       $cache_key
     * @param bool       $use_json
     *
     * @throws Exception
     *
     * @return array|mixed|object
     */
    public static function loadCached(
        $url,
        $cachetime = 86400,
        $entire_item = false,
        $remote_get_args = [],
        $cache_key = null,
        $use_json = false
    ) {
        if (is_string($cachetime)) {
            $cachetime = strtotime($cachetime) - time();
        }

        if (null === $cache_key) {
            $cache_key = __METHOD__.'-'.md5($url);
        }

//        $cache_file = WP_CONTENT_DIR.'/remoteloadercache/'.$cache_key;

        $cache_file = 'cache/remoteloader/'.$cache_key;
        $cache_file = str_replace(['\\', '::', '(', ')'], '/', $cache_file);
        $cache_file = rtrim($cache_file, '/');
        if ($use_json) {
            $cache_file .= '.json';
        } else {
            $cache_file .= '.txt';
        }
        $cache_file = get_theme_file_path($cache_file);

        if (false !== $cachetime) {
            if (static::USE_TRANSIENTS) {
                $cache = get_transient($cache_key);
            } else {
                $cache = false;
            }

            // load by file?
            if (false === $cache && is_file($cache_file) && filemtime($cache_file) + $cachetime > time()) {
                $file = file_get_contents($cache_file);
                if ($use_json) {
                    $cache = json_decode($file, true);
                } else {
                    $cache = maybe_unserialize($file);
                }
            }
            if (is_array($cache)) {
                if ($entire_item) {
                    return $cache;
                }

                return $cache['data'];
            }
        }

//        if (WP_DEBUG) {
        $response = wp_remote_get($url);
//        } else {
//            $response = wp_safe_remote_get( $url );
//        }
        $cache = [
            'url' => $url,
            'modified' => null,
            // will be an array,
            'data' => null,

            // will be a string if it encounters an error
            'decode_error' => null,
        ];
        if (!$use_json) {
            $cache['response'] = $response;
        }

        if (!is_wp_error($response)) {
            $content_type = wp_remote_retrieve_header($response, 'content-type');
            $content_type = preg_replace('@\s+@', '', $content_type);
            $content_type = strtolower($content_type);

            $is_json = 'application/json' === $content_type;
            $is_xml = in_array($content_type, ['text/xml', 'application/xml'], false)
                       //                       || strpos( $content_type, '/xml+' )
                       || preg_match('/\+xml$/i', $content_type);

            $is_html = ($content_type = 'text/html') || 0 === strpos($content_type, 'text/html;charset');

            if ($is_json) {
                $body = wp_remote_retrieve_body($response);

                $data = json_decode($body, true);

                if (!json_last_error()) {
                    $cache['data'] = $data;
                } else {
                    $cache['decode_error'] = 'Error loading JSON: '.json_last_error().':'.json_last_error_msg();
                }
            } elseif ($is_html) {
                $cache['data'] = wp_remote_retrieve_body($response);
            } elseif ($is_xml/* || $is_html */) {
                // todo: test xml/html conversion.
                $body = wp_remote_retrieve_body($response);

                $handler = function ($errno, $errstr) {
                    if (E_WARNING === $errno && (substr_count($errstr, 'DOMDocument::load') > 0)) {
                        throw new \DOMException($errstr);
                    }

                    return false;
                };

                if (seems_utf8($body)) {
                    $encoding = 'UTF-8';
                } else {
                    $encoding = mb_detect_encoding($body);
                }
                // todo: encoding tests??

                try {
                    set_error_handler($handler);
                    $doc = new \DOMDocument('1.0', $encoding);
                    if ($is_html) {
                        $doc->loadHTML($body);
                    } else {
                        $doc->loadXML($body);
                    }
                    $cache['data'] = $doc;
                } catch (\Exception $e) {
                    $cache['decode_error'] = 'Error loading XML: '.$e->getMessage();
                }
                restore_error_handler();
            }
        }

        $cache['timestamp'] = time();
        $cache['modified'] = wp_date(get_option('date_format').' H:i:s (e)');

        if (false !== $cachetime) {
            $status = wp_remote_retrieve_response_code($response);
            if ('' === $status || $status < 200 || $status > 299) {
                $expires = WP_DEBUG ? 1 : 60;
            } elseif (null !== $cache['data']) {
                $expires = $cachetime;
            } elseif (WP_DEBUG) {
                $expires = 1;
            } else {
                // retry in a minute.
                $expires = 60;
            }

            if ($use_json) {
                $serialized = json_encode($cache, JSON_PRETTY_PRINT);
            } else {
                $serialized = serialize($cache);
            }
            $do_file_based = true;
            if (static::USE_TRANSIENTS && strlen($serialized) < 1024 * 1024) {
                $do_file_based = !set_transient($cache_key, $cache, $expires);
            }
            if ($do_file_based) {
                $dir = dirname($cache_file);
                if (!is_dir($dir)) {
                    mkdir($dir, 0777, true);
                }
                file_put_contents($cache_file, $serialized);
            }
        }

        if ($entire_item) {
            return $cache;
        }

        return $cache['data'];
    }
}
