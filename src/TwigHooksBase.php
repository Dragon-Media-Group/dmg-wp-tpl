<?php

/*
 * Copyright 2021 Dragon Media Group.
 * All rights reserved. Modification of this file may only be done using written permission.
 */

namespace DMG\Wordpress\Template;

use DMG\Wordpress\Template\Twig\DumpExtension;
use DOMDocument;
use DOMElement;
use InvalidArgumentException;
use Timber\Image;
use Timber\ImageHelper;
use Twig\Environment;
use Twig\Extension\DebugExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class TwigHooksBase
{
    protected $colors
        = [
            'aliceblue' => '#f0f8ff',
            'antiquewhite' => '#faebd7',
            'aqua' => '#00ffff',
            'aquamarine' => '#7fffd4',
            'azure' => '#f0ffff',
            'beige' => '#f5f5dc',
            'bisque' => '#ffe4c4',
            'black' => '#000000',
            'blanchedalmond' => '#ffebcd',
            'blue' => '#0000ff',
            'blueviolet' => '#8a2be2',
            'brown' => '#a52a2a',
            'burlywood' => '#deb887',
            'cadetblue' => '#5f9ea0',
            'chartreuse' => '#7fff00',
            'chocolate' => '#d2691e',
            'coral' => '#ff7f50',
            'cornflowerblue' => '#6495ed',
            'cornsilk' => '#fff8dc',
            'crimson' => '#dc143c',
            'cyan' => '#00ffff',
            'darkblue' => '#00008b',
            'darkcyan' => '#008b8b',
            'darkgoldenrod' => '#b8860b',
            'darkgray' => '#a9a9a9',
            'darkgreen' => '#006400',
            'darkgrey' => '#a9a9a9',
            'darkkhaki' => '#bdb76b',
            'darkmagenta' => '#8b008b',
            'darkolivegreen' => '#556b2f',
            'darkorange' => '#ff8c00',
            'darkorchid' => '#9932cc',
            'darkred' => '#8b0000',
            'darksalmon' => '#e9967a',
            'darkseagreen' => '#8fbc8f',
            'darkslateblue' => '#483d8b',
            'darkslategray' => '#2f4f4f',
            'darkslategrey' => '#2f4f4f',
            'darkturquoise' => '#00ced1',
            'darkviolet' => '#9400d3',
            'deeppink' => '#ff1493',
            'deepskyblue' => '#00bfff',
            'dimgray' => '#696969',
            'dimgrey' => '#696969',
            'dodgerblue' => '#1e90ff',
            'firebrick' => '#b22222',
            'floralwhite' => '#fffaf0',
            'forestgreen' => '#228b22',
            'fuchsia' => '#ff00ff',
            'gainsboro' => '#dcdcdc',
            'ghostwhite' => '#f8f8ff',
            'gold' => '#ffd700',
            'goldenrod' => '#daa520',
            'gray' => '#808080',
            'green' => '#008000',
            'greenyellow' => '#adff2f',
            'grey' => '#808080',
            'honeydew' => '#f0fff0',
            'hotpink' => '#ff69b4',
            'indianred' => '#cd5c5c',
            'indigo' => '#4b0082',
            'ivory' => '#fffff0',
            'khaki' => '#f0e68c',
            'lavender' => '#e6e6fa',
            'lavenderblush' => '#fff0f5',
            'lawngreen' => '#7cfc00',
            'lemonchiffon' => '#fffacd',
            'lightblue' => '#add8e6',
            'lightcoral' => '#f08080',
            'lightcyan' => '#e0ffff',
            'lightgoldenrodyellow' => '#fafad2',
            'lightgray' => '#d3d3d3',
            'lightgreen' => '#90ee90',
            'lightgrey' => '#d3d3d3',
            'lightpink' => '#ffb6c1',
            'lightsalmon' => '#ffa07a',
            'lightseagreen' => '#20b2aa',
            'lightskyblue' => '#87cefa',
            'lightslategray' => '#778899',
            'lightslategrey' => '#778899',
            'lightsteelblue' => '#b0c4de',
            'lightyellow' => '#ffffe0',
            'lime' => '#00ff00',
            'limegreen' => '#32cd32',
            'linen' => '#faf0e6',
            'magenta' => '#ff00ff',
            'maroon' => '#800000',
            'mediumaquamarine' => '#66cdaa',
            'mediumblue' => '#0000cd',
            'mediumorchid' => '#ba55d3',
            'mediumpurple' => '#9370db',
            'mediumseagreen' => '#3cb371',
            'mediumslateblue' => '#7b68ee',
            'mediumspringgreen' => '#00fa9a',
            'mediumturquoise' => '#48d1cc',
            'mediumvioletred' => '#c71585',
            'midnightblue' => '#191970',
            'mintcream' => '#f5fffa',
            'mistyrose' => '#ffe4e1',
            'moccasin' => '#ffe4b5',
            'navajowhite' => '#ffdead',
            'navy' => '#000080',
            'oldlace' => '#fdf5e6',
            'olive' => '#808000',
            'olivedrab' => '#6b8e23',
            'orange' => '#ffa500',
            'orangered' => '#ff4500',
            'orchid' => '#da70d6',
            'palegoldenrod' => '#eee8aa',
            'palegreen' => '#98fb98',
            'paleturquoise' => '#afeeee',
            'palevioletred' => '#db7093',
            'papayawhip' => '#ffefd5',
            'peachpuff' => '#ffdab9',
            'peru' => '#cd853f',
            'pink' => '#ffc0cb',
            'plum' => '#dda0dd',
            'powderblue' => '#b0e0e6',
            'purple' => '#800080',
            'red' => '#ff0000',
            'rosybrown' => '#bc8f8f',
            'royalblue' => '#4169e1',
            'saddlebrown' => '#8b4513',
            'salmon' => '#fa8072',
            'sandybrown' => '#f4a460',
            'seagreen' => '#2e8b57',
            'seashell' => '#fff5ee',
            'sienna' => '#a0522d',
            'silver' => '#c0c0c0',
            'skyblue' => '#87ceeb',
            'slateblue' => '#6a5acd',
            'slategray' => '#708090',
            'slategrey' => '#708090',
            'snow' => '#fffafa',
            'springgreen' => '#00ff7f',
            'steelblue' => '#4682b4',
            'tan' => '#d2b48c',
            'teal' => '#008080',
            'thistle' => '#d8bfd8',
            'tomato' => '#ff6347',
            'turquoise' => '#40e0d0',
            'violet' => '#ee82ee',
            'wheat' => '#f5deb3',
            'white' => '#ffffff',
            'whitesmoke' => '#f5f5f5',
            'yellow' => '#ffff00',
            'yellowgreen' => '#9acd32',
        ];

    /**
     * Timber is lame.
     *
     * @filter timber/locations
     *
     * @return string[]
     *
     * @see    \Timber\LocationManager::get_locations()
     */
    public function timberTemplateLocations()
    {
        return array_map('realpath', array_unique([
            get_stylesheet_directory().'/views/',
            get_template_directory().'/views/',
            __DIR__.'/../views/',
        ]));
    }

    /**
     * @filter timber/cache/location
     */
    public function timberCacheLocation()
    {
        return get_theme_file_path('cache/timber/'.md5(__FILE__));
    }

    /**
     * @filter timber/loader/twig
     *
     * @return \Twig\Environment
     */
    public function timberTwig(Environment $twig)
    {
        $dump_extension_added = false;
        if (WP_DEBUG) {
            $twig->enableAutoReload();
            $twig->enableStrictVariables();

            // symfony vardumper
            if (class_exists('HelloNico\Twig\DumpExtension')) {
                $dump_extension_added = true;
//                $twig->addExtension(new Twig\DumpExtension());
                $twig->addExtension(new DumpExtension());
            }

//            $twig->setBaseTemplateClass(AnnotatedTemplate::class);
        } else {
            // test/dev
            if ($_SERVER['REMOTE_ADDR'] === $_SERVER['SERVER_ADDR']
                 || in_array($_SERVER['SERVER_ADDR'], ['127.0.0.1', '::1'], false)
            ) {
                $twig->enableAutoReload();
            } elseif (is_user_logged_in()) {
                $user = wp_get_current_user();
                if ($user instanceof \WP_User
                     && (
                         'ontwikkelaardmg@gmail.com' === $user->user_email
                        || false !== stripos($user->user_email, '@dmg.nu')
                        || false !== stripos($user->user_email, '@dragonmediagroup.nl')
                     )
                ) {
                    $twig->enableAutoReload();
                } else {
                    $twig->disableAutoReload();
                }
            } else {
                $twig->disableAutoReload();
            }

            $twig->disableStrictVariables();
        }

        if (WP_DEBUG && WP_DEBUG_DISPLAY) {
            $twig->enableDebug();
        } else {
            $twig->disableDebug();
        }

        try {
            // doesnt do anything if debug is off,
            // but for forgotten dump commands this is essential.
            if (!$dump_extension_added) {
                $twig->addExtension(new DebugExtension());
            }
        } catch (\Exception $e) {
            // already added? stop exception!
        }

        return $twig;
    }

    /**
     * Timber does not have a filter called timber/twig/globals, so use timber/loader/twig instead.
     *
     * @filter timber/loader/twig
     */
    public function addGlobals(Environment $twig)
    {
        if (function_exists('imagewebp')) {
            $gd_webp_support = function_exists('imagecreatefromwebp');
        } else {
            $gd_webp_support = false;
        }

        $twig->addGlobal('gd_webp_support', $gd_webp_support);

        return $twig;
    }

    /**
     * @filter   timber/twig/filters
     *
     * @priority 999
     *
     * @return Environment
     */
    public function addFilters(Environment $twig)
    {
        $this->registerTwigFunctionAndFilter($twig, 'email_encode', 'emailEncode');
        $this->registerTwigFunctionAndFilter($twig, 'email_link', 'emailLink');
        $this->registerTwigFunctionAndFilter($twig, 'make_block_id', 'makeBlockId');
        $this->registerTwigFunctionAndFilter($twig, 'responsive_video', 'responsiveVideo');
        $this->registerTwigFunctionAndFilter($twig, 'p2br', 'p2br');
        $this->registerTwigFunctionAndFilter($twig, 'resize_fit', 'resizeFit');

        $this->registerTwigFunctionAndFilter($twig, 'yiq');
        $this->registerTwigFunctionAndFilter($twig, 'rgb');
        $this->registerTwigFunctionAndFilter($twig, 'luminance');
        $this->registerTwigFunctionAndFilter($twig, 'best_color');

        $this->registerTwigFunctionAndFilter($twig, 'nav_menu');

        $this->registerTwigFunctionAndFilter($twig, 'base64_resize');

        $twig->addFunction(new TwigFunction(
            'twig_filter_exists',
            [$this, 'filter_exists'],
            ['needs_environment' => true]
        ));
        $twig->addFunction(new TwigFunction(
            'twig_function_exists',
            [$this, 'function_exists'],
            ['needs_environment' => true]
        ));

        return $twig;
    }

    /**
     * @param string $filter_name
     *
     * @return bool
     */
    public function filter_exists(Environment $twig, $filter_name)
    {
        return false !== $twig->getFilter($filter_name);
    }

    /**
     * @param string $function_name
     *
     * @return bool
     */
    public function function_exists(Environment $twig, $function_name)
    {
        return false !== $twig->getFunction($function_name);
    }

    /**
     * @param array|string $menu
     */
    public function nav_menu($menu, array $args = [])
    {
        $defaults = [];
        if (is_array($menu)) {
            $args = array_merge($menu, $args);
        } elseif (is_string($menu) || is_numeric($menu)) {
            $args = array_merge(['menu' => $menu], $args);
        }

        if (isset($args['walker']) && is_string($args['walker'])) {
            $walker = ucfirst($args['walker']);
            $cls = __NAMESPACE__.'\NavWalker\\'.$walker;
            $args['walker'] = new $cls();
            $defaults['fallback_cb'] = "{$cls}::fallback";

            if (0 === stripos($walker, 'bootstrap')) {
                $defaults['container'] = 'div';
                $defaults['depth'] = 2;
                $defaults['container_class'] = 'collapse navbar-collapse';
                $defaults['menu_class'] = 'nav navbar-nav ml-auto';
            }
        }

        $args = array_merge($defaults, $args);
//        dump($args);
        wp_nav_menu($args);
    }

    /**
     * @param string   $baseColor
     * @param string[] $colors
     * @param float    $min_contrast_ratio
     * @param int      $sort
     * @param int      $return_value
     *
     * @return null|array|string
     *
     * @see http://snook.ca/technical/colour_contrast/colour.html#fg=1956FF,bg=FFB6B9
     */
    public function bestColor($baseColor, array $colors, $min_contrast_ratio = 0.0, $sort = 1, $return_value = 1)
    {
        $valid = [];
        $l1 = $this->luminance($baseColor);
        $baseColorRGB = $this->rgb($baseColor);
        foreach ($colors as $index => $color) {
            $colorRGB = $this->rgb($color);
            $l2 = $this->luminance($color);
//            dump($l1, $l2, $color);
            if ($l2 && $l1 !== $l2) {
                if ($l1 >= $l2) {
                    $ratio = ($l1 + .05) / ($l2 + .05);
                } else {
                    $ratio = ($l2 + .05) / ($l1 + .05);
                }
                $ratio = round($ratio * 100) / 100; // round to 2 decimal places
            } else {
                $ratio = 0;
            }

            if ($ratio > $min_contrast_ratio) {
                $valid[] = [
                    'ratio' => $ratio,
                    'index' => $index,
                    'color' => $color,
                    'colorRGB' => $colorRGB,
                    'luminance' => $l2,
                    'baseLuminance' => $l1,
                    'baseColorRGB' => $baseColorRGB,
                ];
            }
        }

        if (!$valid) {
            return null;
        }

        if ($sort) {
            usort($valid, function ($a, $b) use ($sort) {
                if (2 == $sort) {
                    list($a, $b) = [$b, $a];
                }

                if ($a['ratio'] == $b['ratio']) {
                    return $a['index'] < $b['index'];
                }

                return $a['ratio'] < $b['ratio'];
            });
        }

        // return the best color.
        switch ((int) $return_value) {
            case 3:
                return $valid;
            case 2:
                return $valid[0];
            case 1:
                return $valid[0]['index'];
        }

        return $valid[0]['color'];
    }

    /**
     * @param string $color
     *
     * @return float
     *
     * @see https://www.joedolson.com/2008/05/testing-color-contrast/
     */
    public function luminance($color)
    {
        /**
         * @var string
         * @var float  $int
         */
        $l = [];
        foreach ($this->rgb($color) as $c => $float) {
            $float /= 255;
            if ($float <= 0.03928) {
                $l[$c] = $float / 12.92;
            } else {
                $l[$c] = pow(($float + 0.055) / 1.055, 2.4);
            }
        }

        return (0.2126 * $l['r']) + (0.7152 * $l['g']) + (0.0722 * $l['b']);
    }

    /**
     * @param string $color hexolor
     *
     * @return array
     */
    public function rgb($color)
    {
        if (isset($this->colors[$color])) {
            $color = $this->colors[$color];
        }

        if (!preg_match('@^#?([a-f0-9]{3}|[a-f0-9]{6})$$@i', $color)) {
            throw new InvalidArgumentException('Got invalid hex color');
        }
        $color = trim($color, '#');
        if (3 === strlen($color)) {
            $r = hexdec($color[0].$color[0]);
            $g = hexdec($color[1].$color[1]);
            $b = hexdec($color[2].$color[2]);
        } else {
            $r = hexdec(substr($color, 0, 2));
            $g = hexdec(substr($color, 2, 2));
            $b = hexdec(substr($color, 4, 2));
        }

//        dump($color, $r, $g, $b);

        return compact('r', 'g', 'b');
    }

    /**
     * @param $color
     *
     * @return float|int
     */
    public function yiq($color)
    {
        $rgb = $this->rgb($color);

        return (($rgb['r'] * 299) + ($rgb['g'] * 587) + ($rgb['b'] * 114)) / 1000;
    }

    /**
     * @return string
     */
    public function makeBlockId(array $block)
    {
        static $counters;
        if (!isset($block['acf_fc_layout']) || !is_string($block['acf_fc_layout'])) {
            return;
        }

        $type = $block['acf_fc_layout'];

        if (isset($block['id']) && is_scalar($block['id']) && $block['id']) {
            $id = $block['id'];
        } else {
            if (!isset($counters[$type])) {
                $counters[$type] = 0;
            }
            ++$counters[$type];
            $id = $counters[$type];
        }

        return sprintf('%s-%s', $type, $id);
    }

    /**
     * @param string $email
     * @param string $name
     * @param string $params
     *
     * @return string
     */
    public function emailLink($email, $name = '', $params = '')
    {
        $replaced = $this->emailEncode($email);

        if (!$name) {
            $name = $replaced;
        } elseif (filter_var($name, FILTER_VALIDATE_EMAIL)) {
            $name = $this->emailEncode($name);
        }
        if (!$params) {
            $params = 'rel="nofollow"';
        }

        return '<a href="mailto:'.$replaced.'" '.$params.'>'.$name.'</a>';
    }

    /**
     * @param string $email
     *
     * @return string
     */
    public function emailEncode($email)
    {
        $replaced = '';

        $len = strlen($email);
        for ($i = 0; $i < $len; ++$i) {
            $char = $email[$i];
            $r = rand(0, 100);

            // roughly 10% raw, 45% hex, 45% dec
            if ($r > 90) {
                $replaced .= $char;
            } else {
                if ($r < 45) {
                    $replaced .= '&#x'.dechex(ord($char)).';';
                } else {
                    $replaced .= '&#'.ord($char).';';
                }
            }
        }

        return $replaced;
    }

    /**
     * @param string $html
     *
     * @return string
     */
    public function p2br($html)
    {
        $html = preg_replace('@<(p|div)(\\s+[^>]+)?>@i', '', $html);

        return str_ireplace(['</p>', '</div>'], '<br>', $html);
    }

    /**
     * @param string $iframe
     *
     * @return mixed
     */
    public function responsiveVideo($iframe, array $params = [])
    {
        if (false !== strpos($iframe, '<iframe ')) {
            $dom = new DOMDocument();
            $dom->loadHTML($iframe);

            $l = $dom->getElementsByTagName('iframe');
            $el = $el = $l->item(0);
            if ($el instanceof DOMElement) {
                global $wp_query;

                $el->setAttribute('frameborder', 0);

                $el->setAttribute('src', add_query_arg(array_merge([
                    //					'controls'        => 0,
                    //					'hd'              => 1,
                    //					'autohide'        => 1,
                    //					'enablejsapi'     => 1,
                    //					'modestbranding'  => 1,
                    //					'playsinline'     => 0,
                    //					'rel'             => 0,
                    'widget_referrer' => home_url($wp_query),
                    //				'origin'          => get_site_url(),
                ], $params), $el->getAttribute('src')));

                if ($el->hasAttribute('width') && $el->hasAttribute('height')) {
                    $h = $el->getAttribute('height');
                    $w = $el->getAttribute('width');
                    $p = number_format($h / $w * 100, 2, '.', '');

                    $pre = "<div class=\"embed-responsive\" style='padding-top:{$p}%'>";
                    $suf = '</div>';

                    foreach ([21 => 9, 16 => 9, 4 => 3, 1 => 1] as $x => $y) {
                        if ((float) $w / $h === (float) $x / $y) {
                            $pre = "<div class=\"embed-responsive embed-responsive-{$x}by{$y}\">";
                            $suf = '</div>';

                            break;
                        }
                    }
                } else {
                    $pre = '';
                    $suf = '';
                }

                $iframe = $pre.$dom->saveHTML($el).$suf;
            }
        }

        return $iframe;
    }

    /**
     * @param string $src
     * @param int    $w
     * @param int    $h
     *
     * @return bool|string
     */
    public function resizeFit($src, $w, $h = 0)
    {
        $img = new Image($src);
        $src = $img->src();

        // If the image is smaller on both width and height, return original
        if ($img->width() <= $w && $img->height() <= $h) {
            return $src;
        }

        if (0 === $h || $img->aspect() > $w / $h) {
            return ImageHelper::resize($src, $w);
        }

        return ImageHelper::resize($src, 0, $h);
    }

    public function base64_resize($url, $target_mimetype = null, $w = 20, $h = 0, $crop = 'default', $quality = 1)
    {
        $error_image = '';
        if ($url instanceof Image) {
            $url = $url->src();
        }

        $hash = md5(serialize(func_get_args()));
        $cached_file = get_theme_file_path('cache/base64/'.$hash);

        if (file_exists($cached_file)) {
            return file_get_contents($cached_file);
        }

        $response = wp_safe_remote_get($url);

        if (!is_wp_error($response)) {
            $content_type = wp_remote_retrieve_header($response, 'content-type');
            switch ($content_type) {
                case 'image/png':
                    $ext = 'png';

                    break;
                case 'image/gif':
                    $ext = 'gif';

                    break;
                case 'image/jpeg':
                    $ext = 'jpg';

                    break;
                case 'image/webp':
                    $ext = 'webp';

                    break;
                default:
                    return $error_image;
            }
            // save it.
            $temp_file_path = sys_get_temp_dir().'/'.md5($url).'.'.$ext;
            $fh = fopen($temp_file_path, 'wb');
            $body = wp_remote_retrieve_body($response);
            fwrite($fh, $body);
            fclose($fh);

            $editor = wp_get_image_editor($temp_file_path);
            $editor->resize($w, $h, $crop);
            $editor->set_quality($quality);

            if (null === $target_mimetype) {
                $target_mimetype = $content_type;
            }

            $ret = $editor->save(null, $target_mimetype);
            unlink($temp_file_path);

            $dir = dirname($cached_file);
            if (!is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            $fh = fopen($cached_file, 'wb');
            fwrite($fh, 'data:image/');
            fwrite($fh, $ret['mime-type']);
            fwrite($fh, ';base64,');
            fwrite($fh, base64_encode(file_get_contents($ret['path'])));
            fclose($fh);

            unlink($ret['path']);

            return file_get_contents($ret['path']);
        }

        return $error_image;
    }

    /**
     * @filter timber/context
     *
     * @return array
     */
    public function timberContext(array $context = [])
    {
//        $context['gd_webp_support'] = function_exists('imagewebp') && function_exists('imagecreatefromwebp');

        return $context;
    }

    /**
     * @param string               $name
     * @param null|callable|string $callback
     */
    protected function registerTwigFunctionAndFilter(Environment $twig, $name, $callback = null)
    {
        if (null === $callback) {
            $callback = $name;
        }
        if (is_string($callback) && !strpos($callback, '::')) {
            $callback = [$this, $callback];
        }

        $twig->addFunction(new TwigFunction($name, $callback));

        $twig->addFilter(new TwigFilter($name, $callback));
    }
}
