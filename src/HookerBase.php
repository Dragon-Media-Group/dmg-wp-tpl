<?php

/*
 * Copyright 2021 Dragon Media Group.
 * All rights reserved. Modification of this file may only be done using written permission.
 */

namespace DMG\Wordpress\Template;

use ReflectionClass;
use ReflectionMethod;

abstract class HookerBase
{
    public function register($class_or_object, $cached = true)
    {
        $hooks = $this->getHooks($class_or_object, $cached);
        foreach ($hooks as $hook) {
            switch ($hook['type']) {
                case 'action':
                    add_action(
                        $hook['tag'],
                        [$class_or_object, $hook['callback']],
                        $hook['priority'],
                        $hook['num_args']
                    );

                    break;
                case 'filter':
                    add_filter(
                        $hook['tag'],
                        [$class_or_object, $hook['callback']],
                        $hook['priority'],
                        $hook['num_args']
                    );
            }
        }
    }

    public function getHooks($class_or_object, $cached = true)
    {
        $class_name = is_object($class_or_object) ? get_class($class_or_object) : $class_or_object;
        $hooks_file = get_theme_file_path('cache/hooks/'.$class_name.'.json');

        if ($cached && $this->shouldLoadCachedHooks($hooks_file)) {
            $hooks = json_decode(file_get_contents($hooks_file), true);
        } else {
            $hooks = $this->findHooks($class_or_object);
            //			if ( $cached ) {
            $dir = dirname($hooks_file);
            if (!is_dir($dir)) {
                mkdir($dir, 0777, true);
            }

            file_put_contents($hooks_file, json_encode($hooks, WP_DEBUG ? JSON_PRETTY_PRINT : null));
            //			}
        }

        return $hooks;
    }

    /**
     * @param string $file
     *
     * @return bool
     */
    protected function shouldLoadCachedHooks($file)
    {
        return is_file($file) && filemtime(__FILE__) < filemtime($file);
    }

    protected function findHooks($class_or_object)
    {
        $refl = new ReflectionClass(is_object($class_or_object)
            ? get_class($class_or_object)
            : $class_or_object);
        $map = [];
        foreach ($refl->getMethods() as $method) {
            $hook = $this->getHookFromMethod($method);
            if ($hook) {
                $map[] = $hook;
            }
        }

        usort($map, [$this, 'sortHooks']);

        return $map;
    }

    /**
     * @return array|void
     */
    protected function getHookFromMethod(ReflectionMethod $method)
    {
        if (!$method->isPublic()) {
            return;
        }
        if ($method->isAbstract()) {
            return;
        }
        if ($method->isStatic()) {
            return;
        }

        $name = $method->getName();
        $tag = $name;
        if (0 === strpos($name, '__')) {
            return;
        }
        $comment = (string) $method->getDocComment();

        if (preg_match('#@(?<type>action|filter)(?:\s+(?<tag>[^\n\r]+))?#', $comment, $m)) {
            $type = $m['type'];
            if (isset($m['tag']) && '' !== trim($m['tag'])) {
                $tag = trim($m['tag']);
            }
        } else {
            // nothing found, check the base class.
            if (preg_match('#[\b|\s]@{?inheritdoc}?[\b\s]#i', $comment)) {
                $parent_class = $method->getDeclaringClass()->getParentClass();
                if ($parent_class && $parent_class->hasMethod($name)) {
                    return $this->getHookFromMethod($parent_class->getMethod($name));
                }
            }

            return;
        }

        $callback = $name;

        if (preg_match('#@priority\s+([1-9]\d*)#', $comment, $m)) {
            $priority = (int) $m[1];
        } else {
            $priority = 10;
        }
        if (preg_match('#@num_args\s+([1-9]\d*)#', $comment, $m)) {
            $num_args = (int) $m[1];
        } else {
            $num_args = 0;
            foreach ($method->getParameters() as $parameter) {
                if ($parameter->isVariadic()) {
                    $num_args = 9999;

                    break;
                }
                ++$num_args;
            }
        }

        if (!$num_args && 'filter' === $type) {
            $num_args = 1;
        }

        // for debug only
        $declaring_class = $method->getDeclaringClass()->getName();

        return compact('type', 'tag', 'priority', 'callback', 'num_args', 'declaring_class');
    }

    protected function sortHooks(array $a, array $b)
    {
        if ($a['type'] !== $b['type']) {
            return $a['type'] > $b['type'];
        }
        if ($a['priority'] !== $b['priority']) {
            return $a['priority'] > $b['priority'];
        }

        return strnatcmp($a['tag'], $b['tag']);
    }
}
