<?php

/*
 * Copyright 2021 Dragon Media Group.
 * All rights reserved. Modification of this file may only be done using written permission.
 */

namespace DMG\Wordpress\Template;

use Exception;

abstract class ScriptStylesLoaderBase
{
    /** @var string[] */
    protected $manifest;
    /** @var mixed int */
    protected $manifest_modified;
    /** @var string */
    protected $mix_dir;
    /** @var null|string */
    protected $hot;
    /** @var mixed */
    protected $enqueued_block_layouts;
    /** @var string[] */
    protected $base_js_deps;
    /** @var string[] */
    protected $log = [];

    /**
     * @var bool
     */
    private $default_block_sections_enabled = true;

    /**
     * @var bool
     */
    private $default_assets_enabled = true;

    /**
     * ScriptStylesLoaderBase constructor.
     */
    public function __construct()
    {
        $this->enqueued_block_layouts = [];

        $this->mix_dir = get_template_directory().'/dist';
        $manifest_file_template = $this->mix_dir.'/'.$this->getManifestJsonFileName();

        if (!file_exists($manifest_file_template)) {
            $this->log('The manifest file does not exist in the dist dir: '.$this->getManifestJsonFileName());
            $this->manifest = [];
            $this->manifest_modified = time();
            $this->base_js_deps = [];
        } else {
            $modified = filemtime($manifest_file_template);

            $manifest = json_decode(file_get_contents($manifest_file_template), true);
            if (is_child_theme()) {
                $this->mix_dir = get_stylesheet_directory().'/dist';
                $manifest_file_stylesheet = $this->mix_dir.'/'.$this->getManifestJsonFileName();

                if (file_exists($manifest_file_stylesheet)) {
                    $manifest = array_merge(
                        $manifest,
                        json_decode(file_get_contents($manifest_file_stylesheet), true)
                    );

                    $modified = max($modified, filemtime($manifest_file_template));
                }
            }

            if (WP_DEBUG) {
                $hot_file = get_theme_file_path('hot');
                if (file_exists($hot_file)) {
                    $this->hot = trim(trim(file_get_contents($hot_file), '/'));

                    $modified = max($modified, filemtime($hot_file));

                    $this->log('using hot file: '.$hot_file);
                }
            }

            $this->manifest = $manifest;
            $this->manifest_modified = $modified;
            $this->base_js_deps = [];
        }
    }

    /**
     * @return bool
     */
    public function isDefaultAssetsEnabled()
    {
        return $this->default_assets_enabled;
    }

    /**
     * @param bool $default_assets_enabled
     *
     * @return ScriptStylesLoaderBase
     */
    public function setDefaultAssetsEnabled($default_assets_enabled)
    {
        $this->default_assets_enabled = (bool) $default_assets_enabled;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDefaultBlockSectionsEnabled()
    {
        return $this->default_block_sections_enabled;
    }

    /**
     * @param bool $default_block_sections_enabled
     *
     * @return ScriptStylesLoaderBase
     */
    public function setDefaultBlockSectionsEnabled($default_block_sections_enabled = true)
    {
        $this->default_block_sections_enabled = (bool) $default_block_sections_enabled;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getLog()
    {
        return $this->log;
    }

    public function clearLog()
    {
        unset($this->log);
        $this->log = [];
    }

    /**
     * @filter timber/context
     *
     * @return array
     */
    public function triggerACFLoad(array $context)
    {
        if (get_queried_object()) {
            get_field_objects();
        }

        return $context;
    }

    /**
     * @filter timber/context
     *
     * @return array
     */
    public function bindToTwigContext(array $context)
    {
        $context['scriptstylesloader'] = $this;

        return $context;
    }

    /**
     * @filter acf/load_value
     *
     * @param mixed      $value
     * @param int|string $post_id
     *
     * @return mixed
     */
    public function flexibleContentLoadValue($value, $post_id, array $field)
    {
        if (
            $this->default_block_sections_enabled
            && isset($field['parent_layout'])
            && ($parent_layout = $field['parent_layout'])
            && !isset($this->enqueued_block_layouts[$parent_layout])
        ) {
            $parent_field = get_field_object($field['parent']);

            if ($parent_field
                 && 'blocks' === $parent_field['name']
                 && isset($parent_field['layouts'][$parent_layout])) {
                $layout = $parent_field['layouts'][$parent_layout];

                // already ran. output!
                if (did_action('wp_enqueue_scripts')) {
                    $this->enqueueBlockLayoutAssets($layout['name']);
                }

                $this->log("trigger: \"dmg/load_block/{$layout['name']}\"");
                do_action('dmg/load_block/'.$layout['name']);
                $this->log("trigger: \"dmg/load_block\" with \"{$layout['name']}\"");
                do_action('dmg/load_block', $layout['name']);

                $this->enqueued_block_layouts[$field['parent_layout']] = $layout['name'];
            }
        }

        return $value;
    }

    /**
     * @param string          $handle
     * @param string|string[] $path
     * @param null|array      $deps
     * @param bool            $enqueue
     */
    public function js($handle, $path = '', $deps = [], $enqueue = false)
    {
        if (is_array($path)) {
            $deps = (array) $path;
            $path = $handle;
        } elseif ('' === $path) {
            $path = $handle;
        }

        $source_path = $path;

        $path = $this->getCompiledPath($path);
        if (false !== $path) {
            $deps = array_merge($deps, $this->base_js_deps);
            $version = $this->manifest_modified;

            $version = apply_filters('dmg_manifest_version', $version, $handle, $path, $source_path);

            if ($enqueue) {
                wp_enqueue_script($handle, $path, $deps, $version, true);
                $this->log(sprintf('js enqueued: %s: %s@%s (%s)', $handle, $path, $version, __METHOD__));
            } else {
                wp_register_script($handle, $path, $deps, $version, true);
                $this->log(sprintf('js registered: %s: %s@%s (%s)', $handle, $path, $version, __METHOD__));
            }
        } else {
            $this->log(sprintf('js not found: %s (%s)  (path not found)', $handle, __METHOD__));
        }
    }

    /**
     * @param string          $handle
     * @param string|string[] $path
     * @param null|array      $deps
     * @param bool            $enqueue
     */
    public function css($handle, $path = '', $deps = [], $enqueue = false)
    {
        if (is_array($path)) {
            $deps = (array) $path;
            $path = $handle;
        } elseif ('' === $path) {
            $path = $handle;
        }
        $source_path = $path;
        $path = $this->getCompiledPath($path);
        if (false !== $path) {
            $version = $this->manifest_modified;

            $version = apply_filters('dmg_manifest_version', $version, $handle, $path, $source_path);

            if (false === $version || null === $version) {
                $version = 'noversion';
            }

            if ($enqueue) {
                wp_enqueue_style($handle, $path, $deps, $version);
                $this->log(sprintf('css enqueued: %s: %s@%s  (%s)', $handle, $path, $version, __METHOD__));
            } else {
                wp_register_style($handle, $path, $deps, $version);
                $this->log(sprintf('css registered: %s: %s@%s (%s)', $handle, $path, $version, __METHOD__));
            }
        } else {
            $this->log(sprintf('css not found: %s (%s)  (path not found)', $handle, __METHOD__));
        }
    }

    /**
     * @action wp_enqueue_scripts
     */
    public function enqueue()
    {
        if ($this->isDefaultAssetsEnabled()) {
            $theme_script_deps = [];

            try {
                $this->js('manifest.js');
                $theme_script_deps[] = 'manifest.js';
                wp_enqueue_script('manifest.js');
            } catch (Exception $e) {
            }

            try {
                $this->js('vendor.js');
                $theme_script_deps[] = 'vendor.js';
                wp_enqueue_script('vendor.js');
            } catch (Exception $e) {
            }

            $this->css('theme-style', 'style.css');
            $this->js('theme-script', 'script.js', $theme_script_deps);

            wp_enqueue_style('theme-style');
            wp_enqueue_script('theme-script');

            // register block asset files.
            foreach ($this->enqueued_block_layouts as $block_layout_name) {
                $this->enqueueBlockLayoutAssets($block_layout_name);
            }
        }
    }

    /**
     * @filter style_loader_src
     * @priority 9999
     *
     * @param string $src
     *
     * @return string
     */
    public function removeVersionParamStyle($src)
    {
        return $this->removeVersionParamScript($src);
    }

    /**
     * @filter script_loader_src
     * @priority 9999
     *
     * @param string $src
     *
     * @return string
     */
    public function removeVersionParamScript($src)
    {
        if (strpos($src, 'ver=noversion') > 0) {
            $src = remove_query_arg('ver', $src);
        }

        return $src;
    }

    /**
     * @return string
     */
    protected function getManifestJsonFileName()
    {
        return 'mix-manifest.json';
    }

    /**
     * @param string $message
     */
    protected function log($message)
    {
        $this->log[] = $message;
    }

    /**
     * @param $block_layout_name
     */
    protected function enqueueBlockLayoutAssets($block_layout_name)
    {
        $handle = 'theme-block-'.$block_layout_name;
        $js_file = '/blocks/'.$block_layout_name.'.js';
        $css_file = '/blocks/'.$block_layout_name.'.css';

        $m = __METHOD__;
        $this->log("{$m}: {$block_layout_name}");

        $this->js($handle, $js_file, ['theme-script']);
        wp_enqueue_script($handle);
        $this->log("\tenqueued js: {$handle} {$js_file}");

        $this->css($handle, $css_file/*, [ 'theme-style' ] */);
        wp_enqueue_style($handle);
        $this->log("\tenqueued css: {$handle} {$css_file}");
    }

    /**
     * @param string $path
     *
     * @return false|string
     */
    protected function getCompiledPath($path)
    {
        $m = __METHOD__;
        if (0 !== strpos($path, '/')) {
            $path = '/'.$path;
        }

        if (!array_key_exists($path, $this->manifest)) {
            $this->log("{$m}: Unable to locate Mix file: {$path}. Please check your webpack.mix.js output paths and try again.");

            return false;
        }

        if ($this->hot) {
            $compiled_path = rtrim($this->hot, '/').'/'.ltrim($path, '/');
        } else {
            $compiled_path = get_theme_file_uri('dist/'.ltrim($path, '/'));
        }
        $this->log("{$m}: {$path} = {$compiled_path}");

        return $compiled_path;
    }

    /**
     * @param string|string[] $handles
     * @param string|string[] $requirements
     * @param string          $query
     */
    protected function appendStyleDependencies($handles, $requirements, $query = 'registered')
    {
        $this->appendDependencies(wp_styles(), $handles, $requirements, $query);
    }

    /**
     * @param string|string[] $handles
     * @param string|string[] $requirements
     * @param string          $query
     */
    protected function appendDependencies(
        \WP_Dependencies $dependencies,
        $handles,
        $requirements,
        $query = 'registered'
    ) {
        // handles to check.
        if (is_string($handles)) {
            $handles = preg_split('@\s*,\s*]@', $handles);
        }
        // requirements to check.
        if (is_string($requirements)) {
            $requirements = preg_split('@\s*,\s*]@', $requirements);
        }
        foreach ($handles as $handle) {
            $dep_cls = get_class($dependencies);
            $dependency = $dependencies->query($handle, $query);
            if (!$dependency) {
                $this->log("{$dep_cls}: {$handle} not found for query \"{$query}\"");
            } elseif ($dependency instanceof \_WP_Dependency) {
                foreach ($requirements as $requirement) {
                    $dependency->deps[] = $requirement;
                    $this->log("{$dep_cls}: {$requirement} added as requirement for {$handle}, query: \"{$query}\"");
                }
            } else {
                $this->log("???? {$handle} is not a _WP_Dependency!!!!");
            }
        }
    }

    /**
     * @param string|string[] $handles
     * @param string|string[] $requirements
     * @param string          $query
     */
    protected function appendScriptDependencies($handles, $requirements, $query = 'registered')
    {
        $this->appendDependencies(wp_scripts(), $handles, $requirements, $query);
    }
}
