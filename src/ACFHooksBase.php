<?php

/*
 * Copyright 2021 Dragon Media Group.
 * All rights reserved. Modification of this file may only be done using written permission.
 */

namespace DMG\Wordpress\Template;

abstract class ACFHooksBase
{
    protected $fc_queue;

    protected $main_options_page;

    public function __construct()
    {
        foreach (
            [
                //			'acfe-php', // https://wordpress.org/plugins/acf-extended/
                'acf-json',
            ] as $dir
        ) {
            $dir = get_theme_file_path($dir);
            if (!is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
        }

        $this->main_options_page = acf_add_options_page([
            'menu_title' => 'Thema Instellingen',
        ]);

        acf_add_options_page([
            'page_title' => 'Thema Instellingen (algemeen)',
            'menu_title' => 'Algemeen',
            'menu_slug' => 'theme-general-settings',
            'capability' => 'edit_posts',
            //            'redirect' => false,
            'parent_slug' => $this->main_options_page['menu_slug'],
            'post_id' => 'options_general',
        ]);
        acf_add_options_sub_page([
            'page_title' => 'Thema Header/Footer Instellingen',
            'menu_title' => 'Header / Footer',
            'parent_slug' => $this->main_options_page['menu_slug'],
            'post_id' => 'options_header_footer',
        ]);
    }

    /**
     * Loads all the options from the different pages.
     *
     * @filter timber/context
     */
    public function timberContext(array $context)
    {
        $options = [];

        $pages = acf_options_page()->get_pages();

        foreach ($pages as $page) {
            $post_id = $page['post_id'];
            if (strlen($post_id) > 8 && 0 === strpos($post_id, 'options_')) {
                $options[substr($post_id, 8)] = get_fields($post_id);
            }
        }
        $context['options'] = $options;

        return $context;
    }

    /**
     * expands flexible content fields while editing.
     *
     * @action   admin_print_footer_scripts
     * @priority 999
     */
    public function expandFlexibleLayoutFieldsInAdmin()
    {
        $screen = get_current_screen();
        if (!$screen || 'acf-field-group' !== $screen->post_type) {
            return;
        }

        $html = <<<'HTML'
<script>jQuery(function($){
    $('.acf-field-object-flexible-content').addClass('open');
    $('.acf-field-object-flexible-content.open > .settings').css('display','block');
});</script>
HTML;
        echo $html;
    }

    /**
     * @action acf/include_field_types
     */
    public function addFieldTypes2()
    {
        new ACF\MenuFieldType();
        new ACF\ForkAwesomeFieldType();
    }

    /**
     * On loading any post in the admin, remove all the generated image files that timber makes for ACF images.
     * This doesnt affect performance on the frontend and is used to keep the uploads directory size at a minimum.
     *
     * @filter acf/load_value/type=image
     *
     * @param int $image_id
     *
     * TODO: acf image gallery?
     *
     * @return int
     */
    public function cleanTimberImagesOnImageLoadInADmin($image_id)
    {
        if (is_admin()) {
            Site::removeGeneratedTimberImageFiles($image_id);
        }

        return $image_id;
    }

    /**
     * @filter acf/fields/flexible_content/layout_title
     *
     * @param string  $original_title
     * @param mixed[] $field
     * @param mixed[] $layout
     * @param int     $layout_index
     *
     * @return string
     */
    public function filterBlockTitle($original_title, $field, $layout, $layout_index)
    {
        $title = $original_title;

        $title_value = trim((string) get_sub_field('block_title'));
        if ('' === $title_value) {
            $title_value = trim((string) get_sub_field('section_title'));
        }
        if ('' === !$title_value) {
            $title_value = trim((string) get_sub_field('title'));
        }
        if ('' === !$title_value) {
            $title_value = trim((string) get_sub_field('sub_title'));
        }

        if ('' !== $title_value) {
            $title = sprintf(
                '<strong>%s</strong> - <small>type: %s</small>',
                esc_html($title_value),
                esc_html($layout['label'])
            );
        }

        return $title;
    }

    /**
     * @filter acf/location/rule_types
     *
     * @param mixed[] $choices
     *
     * @return mixed[]
     */
    public function addSingularACFFieldGroupLocation(array $choices = [])
    {
        $choices['function_outputs']['is_singular'] = 'is_singular()';

        return $choices;
    }

    /**
     * @filter acf/location/rule_values/is_singular
     *
     * @return array
     */
    public function addSingularACFFieldGroupLocationValues(array $choices = [])
    {
        $choices['true'] = 'true';

        return $choices;
    }

    /**
     * @filter acf/location/rule_match/is_singular
     *
     * @param bool $match
     *
     * @return mixed
     */
    public function addSingularACFFieldGroupLocationMatch($match, array $rule, array $options, array $field_group)
    {
        if ('is_singular' === $rule['param']) {
            $singular = isset($options['post_type'], $options['post_id']);

            switch ($rule['operator']) {
                case '==':
                    $match = $singular;

                    break;
                case '!=':
                    $match = !$singular;

                    break;
            }
        }

        return $match;
    }

    /**
     * @filter acf/load_field
     *
     * @return array
     */
    public function addBootstrap4ButtonClasses(array $field)
    {
        if (isset($field['wrapper']['class']) && 'bs4_btn_class' === $field['wrapper']['class']) {
            $field['choices'] = apply_filters('bs4_btn_variants', [
                'primary' => 'primair',
                'secondary' => 'secundair',
                'success' => 'success',
                'danger' => 'gevaar',
                'warning' => 'waarschuwing',
                'info' => 'info',
                'light' => 'licht',
                'dark' => 'donker',
                'link' => 'link',
            ]);

            $field['instructions']
                = '<a href="https://getbootstrap.com/docs/4.4/components/buttons/">Bootstrap 4 knop stijlen</a>';
        }

        return $field;
    }
}
