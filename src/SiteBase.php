<?php

/*
 * Copyright 2021 Dragon Media Group.
 * All rights reserved. Modification of this file may only be done using written permission.
 */

namespace DMG\Wordpress\Template;

use Timber\FunctionWrapper;
use Timber\ImageHelper;
use Timber\LocationManager;
use Timber\Menu;
use Timber\Post;
use Timber\Site as TimberSite;
use Timber\Term;
use Timber\Timber;
use Timber\User;

abstract class SiteBase extends TimberSite
{
    const BLOG_HOMEPAGE = 'blog-homepage';
    const STATIC_HOMEPAGE = 'static-homepage';
    const STATIC_BLOG = 'static-blog';

    protected $supports_menus = true;
    protected $supports_linebreak_enhancements = false;
    protected $supports_editor_on_post_page = false;

    /**
     * @var string[]
     */
    protected $post_types_that_require_thumbnail = [];

    // todo: translate
    protected $breadcrumbs_before = '<nav id="site-breadcrumbs" role="navigation" aria-label="Broodkruimel">';
    protected $breadcrumbs_after = '</nav>';

    /**
     * @return false|string
     */
    public static function getFrontPageOrBlogType()
    {
        $front_page = is_front_page();
        $home = is_home();
        if ($front_page && $home) {
            return static::BLOG_HOMEPAGE;
        }
        if ($front_page) {
            return static::STATIC_HOMEPAGE;
        }
        if ($home) {
            return static::STATIC_BLOG;
        }

        return false;
    }

    public static function removeGeneratedTimberImageFiles($image_id)
    {
        if (is_numeric($image_id) && class_exists('Timber\ImageHelper')) {
            ImageHelper::delete_attachment($image_id);
        }

        return $image_id;
    }

    /**
     * @return bool
     */
    public function isSupportsEditorOnPostPage()
    {
        return $this->supports_editor_on_post_page;
    }

    /**
     * @param bool $supports_editor_on_post_page
     *
     * @return SiteBase
     */
    public function setSupportsEditorOnPostPage($supports_editor_on_post_page = true)
    {
        $this->supports_editor_on_post_page = (bool) $supports_editor_on_post_page;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSupportsLinebreakEnhancements()
    {
        return $this->supports_linebreak_enhancements;
    }

    /**
     * @param bool $supports_linebreak_enhancements
     *
     * @return SiteBase
     */
    public function setSupportsLinebreakEnhancements($supports_linebreak_enhancements = true)
    {
        $this->supports_linebreak_enhancements = (bool) $supports_linebreak_enhancements;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getPostTypesThatRequireThumbnail()
    {
        return $this->post_types_that_require_thumbnail;
    }

    /**
     * @param string[] $post_types_that_require_thumbnail
     *
     * @return SiteBase
     */
    public function setPostTypesThatRequireThumbnail(array $post_types_that_require_thumbnail)
    {
        $this->post_types_that_require_thumbnail = $post_types_that_require_thumbnail;

        return $this;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function addPostTypeThatRequiresThumbnail($type)
    {
        if (!in_array($type, $this->post_types_that_require_thumbnail, false)) {
            $this->post_types_that_require_thumbnail[] = $type;
        }

        return $this;
    }

    /**
     * @filter body_class
     *
     * @return array
     */
    public function bodyClasses(array $classes)
    {
        $classes[] = static::getFrontPageOrBlogType();

        if (apply_filters('do_remove_home_blog_frontpage_body_classes', true)) {
            $classes = array_combine($classes, $classes);
            unset($classes['home'], $classes['blog']);
        }

        return $classes;
    }

    /**
     * @filter timber/context
     *
     * @return array
     */
    public function timberContext(array $context)
    {
        $queried_object = get_queried_object();

        if ($queried_object instanceof \WP_Post) {
            $context['post'] = new Post($queried_object);
        } // user is the logged in user in default twig...
        elseif ($queried_object instanceof \WP_User) {
            if (get_current_user_id() === $queried_object->ID) {
                $context['author'] = $context['user'];
            } else {
                $context['author'] = new User($queried_object);
            }
        } elseif ($queried_object instanceof \WP_Term) {
            $context['term'] = new Term($queried_object);
        }

        $context['widgets'] = [];
        global $wp_registered_sidebars;
        foreach (array_keys($wp_registered_sidebars) as $sidebar_bar) {
            $context['widgets'][$sidebar_bar] = Timber::get_widgets($sidebar_bar);
        }

        $context['site'] = $this;
        $context['debug'] = WP_DEBUG && WP_DEBUG_DISPLAY;
        $context['queried_object'] = $queried_object;

        $context['wp_head'] = new FunctionWrapper('wp_head', [], true);
        $context['wp_footer'] = new FunctionWrapper('wp_footer', [], true);

        // http://proger.i-forge.net/%D0%9A%D0%BE%D0%BC%D0%BF%D1%8C%D1%8E%D1%82%D0%B5%D1%80/[20121112]%20The%20smallest%20transparent%20pixel.html
        if (false !== strpos($_SERVER['HTTP_USER_AGENT'], ' Chrome/')) {
            $context['inline_pixel'] = 'data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA=';
        } else {
            $context['inline_pixel']
                = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';
        }

        if (function_exists('yoast_breadcrumb')) {
            $context['breadcrumbs'] = yoast_breadcrumb($this->breadcrumbs_before, $this->breadcrumbs_after, false);
        } else {
            $context['breadcrumbs'] = '';
        }

        $context['social'] = get_option('wpseo_social');
        if (!is_array($context['social'])) {
            $context['social'] = [];
        }

        if ($this->isSupportsMenus()) {
            foreach (get_nav_menu_locations() as $location => $menu_id) {
                $context['menus'][$location] = new Menu($menu_id);
            }
        }

        return $context;
    }

    /**
     * @action after_setup_theme
     */
    public function addThemeSupports()
    {
        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');
        add_theme_support('html5', ['comment-form', 'comment-list', 'gallery', 'caption']);
//        add_theme_support('post-formats', ['aside', 'image', 'video', 'quote', 'link', 'gallery', 'audio',]);

        if ($this->isSupportsMenus()) {
            add_theme_support('menus');
        }

        add_theme_support('yoast-seo-breadcrumbs');
    }

    /**
     * @return bool
     */
    public function isSupportsMenus()
    {
        return $this->supports_menus;
    }

    /**
     * @param bool $supports_menus
     *
     * @return SiteBase
     */
    public function setSupportsMenus($supports_menus = true)
    {
        $this->supports_menus = $supports_menus;

        return $this;
    }

    /**
     * @action init
     */
    public function registerPostTypesAndTaxonomies()
    {
    }

    /**
     * Sets the post status to draft if.
     *
     * @action save_post
     *
     * @param int $post_id
     */
    public function setPostAsDraftWhenMissingThumbnail($post_id)
    {
        $post_type = get_post_type($post_id);
        if (!in_array($post_type, $this->getPostTypesThatRequireThumbnail(), false)) {
            return;
        }

        if ('publish' !== get_post_status($post_id)) {
            return;
        }

        if (!has_post_thumbnail($post_id)) {
            // set a transient to show the users an admin message
            set_transient('has_post_thumbnail', 'no');
            $callback = [$this, __FUNCTION__];
            remove_action('save_post', $callback);
            wp_update_post(['ID' => $post_id, 'post_status' => 'draft']);
            add_action('save_post', $callback);
        } else {
            delete_transient('has_post_thumbnail');
        }
    }

    /**
     * @action admin_notices
     */
    public function postMissingThumbnailError()
    {
        if ('no' === get_transient('has_post_thumbnail')) {
            echo "<div class='error notice is-dismissible'><p><strong>Je moet een uitgelichte afbeelding selecteren.</strong><br>Het huidige item is opgeslagen maar kan niet worden gepubliceerd.</p></div>";
            delete_transient('has_post_thumbnail');
        }
    }

    /**
     * When saving a post, remove extraneous timber image files for the post has a thumbnail.
     *
     * @action save_post
     *
     * @param mixed $post_id
     */
    public function cleanTimberImageFilesForPostThumbnailOnSavingPost($post_id)
    {
        $thumbnail_id = get_post_thumbnail_id($post_id);
        if ($thumbnail_id) {
            // call the child class.
            Site::removeGeneratedTimberImageFiles($thumbnail_id);
        }
    }

    /**
     * Scans twig files for templates (edit page/post : template dropdown).
     *
     * @filter theme_templates
     *
     * @param string $post_type
     *
     * @return array
     */
    public function filterThemeTemplates(
        array $post_templates = [],
        \WP_Theme $theme,
        \WP_Post $post = null,
        $post_type
    ) {
        $cache_key = $theme->slug.'-twig-'.$post_type.'-templates';
        $cache_templates = wp_cache_get($cache_key);

        if (false === $cache_templates) {
            // use Timber Location manager
            $template_directories = LocationManager::get_locations();

            $cache_templates = [];
            foreach ($template_directories as $dir) {
                $files = glob("{$dir}/*.twig");
                if ($files) {
                    foreach ($files as $full_path) {
                        $filename = basename($full_path);

                        // @see \WP_Theme::get_post_templates()
                        if (!preg_match('|Template Name:(.*)$|mi', file_get_contents($full_path), $header)) {
                            continue;
                        }

                        $types = ['page'];
                        if (preg_match('|Template Post Type:(.*)$|mi', file_get_contents($full_path), $type)) {
                            $types = explode(',', _cleanup_header_comment($type[1]));
                        }

                        if (in_array($post_type, $types, false)) {
                            $cache_templates[$filename] = _cleanup_header_comment($header[1]);
                        }
                    }
                }
            }
            $expire = $_SERVER['REMOTE_ADDR'] === $_SERVER['SERVER_ADDR']
                ? 1
                : 60 * 15;

            wp_cache_add($cache_key, $cache_templates, 'themes', $expire);
        }

        if ($cache_templates) {
            $post_templates = array_merge($cache_templates, $post_templates);
            ksort($post_templates);
        }

        return $post_templates;
    }

    /**
     * @action edit_form_before_permalink
     */
    public function adminEditFormAfterTitle()
    {
        if ($this->supports_linebreak_enhancements) {
            echo '
            <div>
                <div style="display: inline-block">Gebruik ¶ in de titel om een regeleinde te forceren wanneer de titel op de website verschijnt.</div>
                <div style="display: inline-block">Gebruik {-} (streepje tussen accolades) voor een zacht afbreekstreepje.</div>
            </div>';
        }
    }

    /**
     * @action wp_dashboard_setup
     */
    public function onDashboardSetup()
    {
        if (!is_multisite()
            && current_user_can('upload_files')
            && !get_site_option('upload_space_check_disabled')
        ) {
            wp_add_dashboard_widget(
                'dmg_dashboard_media_disk_quota',
                'DMG Schijfruimte check',
                [$this, 'onDashboardMediaDiskQuota']
            );
        }
    }

    /**
     * @see \wp_dashboard_quota()
     */
    public function onDashboardMediaDiskQuota()
    {
        require_once ABSPATH.WPINC.'/ms-functions.php';

        $q = get_space_allowed();
        $u = get_space_used();

        if ($u > $q) {
            $percentused = '100';
        } else {
            $percentused = ($u / $q) * 100;
        }
        $used = number_format($u, 2, ',', '.');
        $percentused = number_format($percentused, 2, ',', '.');

        printf(
            '<h2>%s/%s MB (%s%%) schijfruimte gebruikt aan bestanden in media</h3>',
            $used,
            $q,
            $percentused
        );
    }

    /**
     * @filter get_space_allowed
     *
     * @param $allowed
     *
     * @return int
     */
    public function onGetSpaceAllowed($allowed)
    {
        if (!is_multisite()) {
            if (defined('WP_SPACE_ALLOWED')) {
                return WP_SPACE_ALLOWED;
            }

            return 1500; // small.
        }

        return $allowed;
    }

    /**
     * @filter the_content
     *
     * @param $content
     *
     * @return string
     */
    public function softHyphensTheContent($content)
    {
        if ($this->supports_linebreak_enhancements) {
            return $this->convertSoftHyphens($content);
        }

        return $content;
    }

    /**
     * @filter wpseo_title
     *
     * @param string $title
     *
     * @return string
     */
    public function softHyphensSeoTitle($title)
    {
        if ($this->supports_linebreak_enhancements) {
            return $this->stripSoftHyphensAndHardBreaks($title);
        }

        return $title;
    }

    /**
     * @param $title
     *
     * @return mixed
     */
    public function stripSoftHyphensAndHardBreaks($title)
    {
        if (!$this->supports_linebreak_enhancements || is_admin()) {
            return $title;
        }

        return str_replace(['¶', '{-}'], '', $title);
    }

    /**
     * @filter wp_title
     *
     * @param string $title
     *
     * @return string
     */
    public function softHyphensWpTitle($title)
    {
        if ($this->supports_linebreak_enhancements) {
            return $this->stripSoftHyphensAndHardBreaks($title);
        }

        return $title;
    }

    /**
     * @filter the_title
     *
     * @param string $title
     *
     * @return string
     */
    public function softHyphensTheTitle($title)
    {
        if (!$this->supports_linebreak_enhancements || is_admin()) {
            return $title;
        }
        $title = $this->convertSoftHyphens($title);

        return str_replace('¶', '<br class="forced-pillcrow">', $title);
    }

    /**
     * Removes the notice for when on the posts page (editor missing)
     * and restores the editor. PRIORITY MUST BE 0!
     *
     * @action   edit_form_after_title
     * @priority 0
     */
    public function addBackEditorToPostsPage(\WP_Post $post)
    {
        if (!$this->supports_editor_on_post_page || get_option('page_for_posts') !== $post->ID) {
            return;
        }
        if (WP_DEBUG_DISPLAY && !function_exists('_wp_posts_page_notice')) {
            echo '<div><strong>_wp_posts_page_notice</strong> was removed, please edit ', __METHOD__, ',!</div>';

            return;
        }

        remove_action('edit_form_after_title', '_wp_posts_page_notice');
        add_post_type_support('page', 'editor');
    }

    /**
     * @filter display_post_states
     *
     * @param mixed[] $states
     *
     * @return mixed[]
     *
     * @see    \get_post_states()
     */
    public function addTemplateNameAsPostState(array $states, \WP_Post $post)
    {
        $slug = get_page_template_slug($post);
        if (is_string($slug) && '' !== $slug) {
            $templates = get_page_templates($post);
            $template = array_search($slug, $templates, false);
            if (false !== $template) {
                $states['template'] = sprintf('Template: %s', $template);
            }
        }

        return $states;
    }

    /**
     * Template filter.
     *
     * @action restrict_manage_posts
     *
     * @param string $post_type the post type slug
     * @param string $which     the location of the extra table nav markup:
     *                          'top' or 'bottom' for WP_Posts_List_Table,
     *                          'bar' for WP_Media_List_Table
     *
     * @see    WP_Posts_List_Table::extra_tablenav
     */
    public function displayTemplateFilter($post_type, $which)
    {
        $template = isset($_GET['template_filter']) ? $_GET['template_filter'] : '';

        $templates = get_page_templates(null, $post_type);
        if ($templates) {
            echo '<label class="screen-reader-text" for="filter-by-template">Op template filteren</label>';
            echo '<select name="template_filter" id="filter-by-template">';
            echo '<option value="">Alle templates</option>';
            page_template_dropdown($template, $post_type);
            echo '</select>';
        }
    }

    /**
     * Filter request by page template.
     *
     * @filter request
     *
     * @param mixed[] $vars
     *
     * @return mixed[]
     */
    public function filterAdminRequestByTemplate(array $vars = [])
    {
        if (is_admin()) {
            $screen = get_current_screen();

            // post list check
            if ($screen && $screen->post_type && '' === $screen->action && 'edit' === $screen->base) {
                $template = isset($_GET['template_filter']) && is_string($_GET['template_filter'])
                    ? $_GET['template_filter'] : '';

                // did we select a template?
                if ('' !== $template) {
                    $vars['meta_query'][] = [
                        'key' => '_wp_page_template',
                        'value' => $template,
                        'compare' => '=',
                    ];
                }
            }
        }

        return $vars;
    }

    /**
     * @param string $title
     *
     * @return string
     */
    protected function convertSoftHyphens($title)
    {
        return str_replace('{-}', '&#173;', $title);
    }
}
