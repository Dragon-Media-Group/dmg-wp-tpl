<?php

/*
 * Copyright 2021 Dragon Media Group.
 * All rights reserved. Modification of this file may only be done using written permission.
 */

namespace DMG\Wordpress\Template\NavWalker;

abstract class Bootstrap4Base extends \Walker_Nav_Menu
{
    /**
     * @var bool
     */
    private $dropdown = false;

    /**
     * @param array $args
     */
    public static function fallback($args)
    {
        if (current_user_can('edit_theme_options')) {
            $defaults = [
                'container' => 'div',
                'container_id' => false,
                'container_class' => false,
                'menu_class' => 'menu',
                'menu_id' => false,
            ];
            $args = wp_parse_args($args, $defaults);
            if (!empty($args['container'])) {
                echo sprintf(
                    '<%s id="%s" class="%s">',
                    $args['container'],
                    $args['container_id'],
                    $args['container_class']
                );
            }
            echo sprintf('<ul id="%s" class="%s">', $args['container_id'], $args['container_class']).
                 '<li class="nav-item">'.
                 '<a href="'.admin_url('nav-menus.php').'" class="nav-link">'.__('Add a menu').'</a>'.
                 '</li></ul>';
            if (!empty($args['container'])) {
                echo sprintf('</%s>', $args['container']);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function start_lvl(&$output, $depth = 0, $args = [])
    {
        if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $this->dropdown = true;
        $output .= $n.str_repeat($t, $depth).'<div class="dropdown-menu" role="menu">'.$n;
    }

    /**
     * {@inheritdoc}
     */
    public function end_lvl(&$output, $depth = 0, $args = [])
    {
        if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $this->dropdown = false;
        $output .= $n.str_repeat($t, $depth).'</div>'.$n;
    }

    /**
     * {@inheritdoc}
     */
    public function start_el(&$output, $item, $depth = 0, $args = [], $id = 0)
    {
        if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent = str_repeat($t, $depth);

        if ($this->dropdown
             && (
                 preg_match('@^[-_]+$@', trim($item->title))
                 || 0 === strcasecmp($item->title, 'divider')
                 || 0 === strcasecmp($item->attr_title, 'divider')
             )
        ) {
            $output .= $indent.'<div class="dropdown-divider"></div>'.$n;

            return;
        }
        $classes = empty($item->classes) ? [] : (array) $item->classes;
        $classes[] = 'menu-item-'.$item->ID;
        $classes[] = 'nav-item';
        if ($args->walker->has_children) {
            $classes[] = 'dropdown';
        }
        if (0 < $depth) {
            $classes[] = 'dropdown-menu';
        }
        /**
         * Filters the arguments for a single nav menu item.
         *
         * @param \stdClass $args  an object of wp_nav_menu() arguments
         * @param \WP_Post  $item  menu item data object
         * @param int       $depth Depth of menu item. Used for padding.
         *
         * @since 1.2.0
         */
        $args = apply_filters('nav_menu_item_args', $args, $item, $depth);
        /**
         * Filters the CSS class(es) applied to a menu item's list item element.
         *
         * @param array     $classes the CSS classes that are applied to the menu item's `<li>` element
         * @param \WP_Post  $item    the current menu item
         * @param \stdClass $args    an object of wp_nav_menu() arguments
         * @param int       $depth   Depth of menu item. Used for padding.
         *
         * @since 3.0.0
         * @since 4.1.0 The `$depth` parameter was added.
         */
        $class_names = join(
            ' ',
            apply_filters('nav_menu_css_class', array_filter($classes), $item, $args, $depth)
        );
        $class_names = $class_names ? ' class="'.esc_attr($class_names).'"' : '';
        /**
         * Filters the ID applied to a menu item's list item element.
         *
         * @param string    $menu_id the ID that is applied to the menu item's `<li>` element
         * @param \WP_Post  $item    the current menu item
         * @param \stdClass $args    an object of wp_nav_menu() arguments
         * @param int       $depth   Depth of menu item. Used for padding.
         *
         * @since 3.0.1
         * @since 4.1.0 The `$depth` parameter was added.
         */
        $li_id = apply_filters('nav_menu_item_id', 'menu-item-'.$item->ID, $item, $args, $depth);
        $li_id = $li_id ? ' id="'.esc_attr($li_id).'"' : '';
        if (!$this->dropdown) {
            $output .= $indent.'<li'.$li_id.$class_names.'>'.$n.$indent.$t;
        }
        $atts = [];
        $atts['title'] = !empty($item->attr_title) ? $item->attr_title : '';
        $atts['target'] = !empty($item->target) ? $item->target : '';
        $atts['rel'] = !empty($item->xfn) ? $item->xfn : '';
        $atts['href'] = !empty($item->url) ? $item->url : '';
        /**
         * Filters the HTML attributes applied to a menu item's anchor element.
         *
         * @param array $atts {
         *                    The HTML attributes applied to the menu item's `<a>` element, empty strings are ignored
         *
         * @var string $title  title attribute
         * @var string $target target attribute
         * @var string $rel    the rel attribute
         * @var string $href   The href attribute.
         *             }
         *
         * @param \WP_Post  $item  the current menu item
         * @param \stdClass $args  an object of wp_nav_menu() arguments
         * @param int       $depth Depth of menu item. Used for padding.
         *
         * @since 3.6.0
         * @since 4.1.0 The `$depth` parameter was added.
         */
        $atts = apply_filters('nav_menu_link_attributes', $atts, $item, $args, $depth);
        if ($args->walker->has_children) {
            $atts['data-toggle'] = 'dropdown';
            $atts['aria-haspopup'] = 'true';
            $atts['aria-expanded'] = 'false';
        }
        $attributes = '';
        foreach ($atts as $attr => $value) {
            if (!empty($value)) {
                $value = ('href' === $attr) ? esc_url($value) : esc_attr($value);
                $attributes .= ' '.$attr.'="'.$value.'"';
            }
        }
        /** This filter is documented in wp-includes/post-template.php */
        $title = apply_filters('the_title', $item->title, $item->ID);
        /**
         * Filters a menu item's title.
         *
         * @param string    $title the menu item's title
         * @param \WP_Post  $item  the current menu item
         * @param \stdClass $args  an object of wp_nav_menu() arguments
         * @param int       $depth Depth of menu item. Used for padding.
         *
         * @since 4.4.0
         */
        $title = apply_filters('nav_menu_item_title', $title, $item, $args, $depth);
        $item_classes = ['nav-link'];
        if ($args->walker->has_children) {
            $item_classes[] = 'dropdown-toggle';
        }
        if (0 < $depth) {
            $item_classes = array_diff($item_classes, ['nav-link']);
            $item_classes[] = 'dropdown-item';
        }
        $item_output = $args->before;
        $item_output .= '<a class="'.implode(' ', $item_classes).'" '.$attributes.'>';
        $item_output .= $args->link_before.$title.$args->link_after;
        $item_output .= '</a>';
        $item_output .= $args->after;
        /*
         * Filters a menu item's starting output.
         *
         * The menu item's starting output only includes `$args->before`, the opening `<a>`,
         * the menu item's title, the closing `</a>`, and `$args->after`. Currently, there is
         * no filter for modifying the opening and closing `<li>` for a menu item.
         *
         * @param   string     $item_output  The menu item's starting HTML output.
         * @param   \WP_Post   $item         Menu item data object.
         * @param   int        $depth        Depth of menu item. Used for padding.
         * @param   \stdClass  $args         An object of wp_nav_menu() arguments.
         *
         * @since 3.0.0
         *
         */
        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
    }

    /**
     * {@inheritdoc}
     */
    public function end_el(&$output, $item, $depth = 0, $args = [])
    {
        if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $output .= $this->dropdown ? '' : str_repeat($t, $depth).'</li>'.$n;
    }
}
