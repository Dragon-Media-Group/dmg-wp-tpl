<?php
/*
 * This document has been generated with
 * https://mlocati.github.io/php-cs-fixer-configurator/#version:2.15|configurator
 * you can change this configuration by importing this file.
 */

$year = date('Y');

return PhpCsFixer\Config::create()
	->setRules([
		'@PSR2' => true,
		'@PhpCsFixer' => true,
		// PHP arrays should be declared using the configured syntax.
		'array_syntax' => ['syntax' => 'short'],
		// Add, replace or remove header comment.
		'header_comment' => ['header' => "Copyright $year Dragon Media Group.\nAll rights reserved. Modification of this file may only be done using written permission."],
		// Ensure there is no code on the same line as the PHP open tag.
		'linebreak_after_opening_tag' => true,

	])
	->setFinder(PhpCsFixer\Finder::create()
		->exclude('vendor')
		->in(__DIR__)
	);